/************************************************************************//**
* @file			ir.c
*
* @brief		This module contains the IR devices Operations.
*
* @attention	Copyright 2015 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*		os_evt_wait_or		disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/12/15 \n by \Sonam 
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			 
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/	
	void off(void);
	void on(void); 

/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
   void on()
           
		  {
		 datasend(900,450);
	     transmit(0xC3);
		 transmit(0xEA);
	     transmit(0x07);
	     transmit(0x00);
	     transmit(0x04);
	     transmit(0x00);
         transmit(0x04);
		 transmit(0x00);
	     transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x5A);
		 datasend(60,50);
		 }
/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/ 
       void off()
         {
	     datasend(900,450);
		 transmit(0xC3);
		 transmit(0xE2);
	  	 transmit(0x07);
	   	 transmit(0x00);
	  	 transmit(0x04);
	     transmit(0x00);
     	 transmit(0x04);
		 transmit(0x00);
	     transmit(0x00);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0xA0);
		 transmit(0xF4);
         datasend(60,50);
		// GPIO_ResetBits(GPIOC,GPIO_Pin_0);	
	}

/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
       void increase20()
		 {
		 datasend(900,450);
		 transmit(0xC3);
		 transmit(0xE6);
		 transmit(0x07);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x56);
		 datasend(60,50);
		  }
/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/			
	   void increase22()
		 {
		 datasend(900,450);
		 transmit(0xC3);
		 transmit(0xEE);
		 transmit(0x07);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x5E);
		 datasend(60,50);
		  }
/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/			
	   void increase24()
		 {
		 datasend(900,450);
		 transmit(0xC3);
		 transmit(0xE1);
		 transmit(0x07);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x51);
		 datasend(60,50);
		  }
/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
		void increase26()
		 {
		 datasend(900,450);
		 transmit(0xC3);
		 transmit(0xE9);
		 transmit(0x07);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x59);
		 datasend(60,50);
		  }
/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
		void increase25()
		 {
		 datasend(900,450);
		 transmit(0xC3);
		 transmit(0xF1);
		 transmit(0x07);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x49);
		 datasend(60,50);
		  }
/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
	    void decrease()
      	{
	   	 datasend(900,450);
	     transmit(0xC3);
		 transmit(0xF5);
		 transmit(0x07);
		 transmit(0x00);
		 transmit(0x05);
		 transmit(0x00);
		 transmit(0x02);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x80);
		 transmit(0xCA);
		 datasend(60,50);
	    }

/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
				void sleep()
      	{
	   	 datasend(900,450);
	     transmit(0xC3);
		 transmit(0xFE);
		 transmit(0x07);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0x24);
		 transmit(0x00);
		 transmit(0x00);
		 transmit(0x04);
		 transmit(0x00);
		 transmit(0xD0);
		 transmit(0x89);
		 datasend(60,50);
	    }

