#include <stdint.h>
#include <stdlib.h>
#include "io_system.h"
#include "port_mapping.h"
#include "pin_config.h"
#include "memory_info.h"
#include "timer.h"
#include "eeprom.h"
#include "system.h"

extern struct input_port_mapping inps[MAX_INPUT_PORTS];
extern struct port_param *op_port_mem_pool_head;
extern struct system_params sys_info;

/********************************************************************************************************************************
 * Function name: 	void chk_if_pulse_to_be_sent (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine checks if a pulse is to be sent or not. If yes then transmit a pulse with ON period of 1 sec.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void chk_if_pulse_to_be_sent (void) {
	/* Pointer to traverse the output port pool. */
	struct port_param *traverse = op_port_mem_pool_head;
	/* Traverse the whole output pool. */
	while (traverse) {
		/* Check if current output port is of DO PULSE type. */
		if (traverse->output_port_type == OUTPUT_PORT_TYPE_DO_PULSE) {
			/* Check if pulse transmission is still in progress or not. */
			if (traverse->send_pulse == PORT_STATE_ON) {
				if (traverse->pulse_state) {
					set_do (traverse->port, traverse->pulse_state = PORT_STATE_OFF);
					traverse->send_pulse = PORT_STATE_OFF;
				}
				else {
					set_do (traverse->port, traverse->pulse_state = PORT_STATE_ON);
				}
			}
		}
		/* Move on to the next port. */
		traverse = traverse->next_port;
	}
}

/********************************************************************************************************************************
 * Function name: 	void check_feedback (uint8_t port)
 *
 * Returns: 		None
 *
 * Arguments: 		uint8_t port			output port number to be checked if it is associated with given feedback output port.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine checks if a pulse is to be sent or not on a given output port. If yes then set the send pulse flag.
 *
 * Notes:			None
 *******************************************************************************************************************************/
uint8_t check_fb_if_state_to_be_changed (uint8_t port, uint8_t state_to_be_set) {
	uint8_t idx;
	
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		/* Checking all inputs for feedback type. */
		if (inps [idx].input_port_type == INPUT_PORT_TYPE_FEEDBACK) {
			/* Checking if the output port mapped with current feedback input is on the current slave. */
			if (inps [idx].port_pool->port_struct->slave_add == 2) {
				/* Checking if the output port mapped with current feedback input and the output port whose feedback is to be checked match. */
				if (inps [idx].port_pool->port_struct->port == port) {
					/* Checking if the output port mapped with current feedback input is of pulse type. */
					if (inps [idx].port_pool->port_struct->output_port_type == OUTPUT_PORT_TYPE_DO_PULSE) {
						/* If port state & feedback logic level match, it means that a pulse is to be sent. (ON state means 0 logic level & vice versa.) */
						if (state_to_be_set == read_input_pin (idx)) {
//							/* Check if any other binded PIR inputs still have non zero duration or not. */
//							if (!check_binded_pir_input_duration (inps [idx].binding_key)) {
								uint8_t perm;
								/* Check If Feedback Based Pulse Output Control is Permitted Or Not.*/
								eeprom_data_read_write(FB_BSD_OP_CNTRL_PERM(idx), READ_OP, &perm, sizeof (perm));
								if (perm == PERMISSION_ALLOWED) {
									/* Update the send pulse flag for selected output port. */
									inps [idx].port_pool->port_struct->send_pulse = PORT_STATE_ON;
									return 1;
								}
//							}
						}
					}
				}
			}
		}
	}
	return 0;
}

/********************************************************************************************************************************
 * Function name: 	void execute_op_ops (struct ports_pool *src_pool, uint8_t state_to_be_set)
 *
 * Returns: 		None
 *
 * Arguments: 		struct ports_pool *src_pool		Structure to output port pool attached to a specific input.
 *					uint8_t state_to_be_set			PORT_STATE_ON / PORT_STATE_OFF
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to execute port operations on attached output ports of inputs.
 *
 * Notes:			
 *******************************************************************************************************************************/
void execute_op_ops (struct ports_pool *src_pool, uint8_t state_to_be_set) {
	/* Scanning through output port pool of current input port. */
	while (src_pool) {
		switch (src_pool->port_struct->output_port_type) {
			case OUTPUT_PORT_TYPE_DO:
				/* The port exists on this slave. */
				if (src_pool->port_struct->slave_add == 2) {
					set_do (src_pool->port_struct->port, state_to_be_set);
					eeprom_data_read_write(OUTPUT_STATE_SAVED(src_pool->port_struct->port - 64), WRITE_OP, &state_to_be_set, sizeof (state_to_be_set));
				}
				src_pool->port_struct->value = state_to_be_set;
				break;
			case OUTPUT_PORT_TYPE_DO_PULSE:
				/* Proceed only if system is in Automatic Mode. */
				if (sys_info.sys_mode == MODE_AUTOMATIC) {
					/* The port exists on this slave. */
					if (src_pool->port_struct->slave_add == 2) {
						/* Checking feedback for current port & determining if a pulse is required or not. */
						if (check_fb_if_state_to_be_changed (src_pool->port_struct->port, state_to_be_set)) {
							/* Update state only if all other binded inputs have 0 durations. */
							src_pool->port_struct->value = state_to_be_set;
						}
					}
				}
				break;
			case OUTPUT_PORT_TYPE_AO:
				break;
		}
		/* Traverse Port Pool. */
		src_pool = src_pool->next_port_struct;
	}
}

/********************************************************************************************************************************
 * Function name: 	void clear_pir_input_port_parameters (uint8_t ip_idx, struct pir_params *src_info)
 *
 * Returns: 		None
 *
 * Arguments: 		struct pir_params *src_info		Pointer to pir_params type structure
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to clear pir_paramstype node connected to a pir input. 
 *
 * Notes:			
 *******************************************************************************************************************************/
void clear_pir_input_port_parameters (struct pir_params *src_info) {
	src_info -> sensor_count			= 0;		/*	Number of activities sensed by pir sensor. */
	src_info -> activity_level			= 0;		/*	Activity level to be calculated at the end of sampling window. */
	src_info -> sense_window			= 0;		/*	Counter used to count number of seconds elapsed after which,
														we poll pir sensor. */
	src_info -> null_window_counter		= 0;		/*	Counter used to count number of null windows encountered after which,
														time decrement occurs. */
	/* ON Duration after which OFF operation has to be executed on all the associated output ports of current input. */
	src_info -> on_duration				= 0;
}

/********************************************************************************************************************************
 * Function name: 	void set_binded_pir_input_duration (uint8_t key, uint8_t port_state)
 *
 * Returns: 		None
 *
 * Arguments: 		struct ports_pool *src_pool		Structure to output port pool attached to a specific input.
 *					uint8_t state_to_be_set			PORT_STATE_ON / PORT_STATE_OFF
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to set pir_operation duration for binded pir type input.
 *
 * Notes:		
 *******************************************************************************************************************************/
void set_binded_pir_input_duration (uint8_t key, uint8_t port_state) {
	uint8_t idx;
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		if (inps [idx].input_port_type == INPUT_PORT_TYPE_PIR) {
			if (inps [idx].binding_key == key) {
				if (port_state) {
					inps [idx].pir_info->on_duration += MINS(30);
				}
				else {
					clear_pir_input_port_parameters (inps [idx].pir_info);
				}
			}
		}
	}
}

/********************************************************************************************************************************
 * Function name: 	uint8_t check_binded_pir_input_duration (uint8_t key)
 *
 * Returns: 		uint8_t							Port State i.e.:- PORT_STATE_ON/PORT_STATE_OFF
 *
 * Arguments: 		uint8_t key						Binding key of pir type input.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to check pir_operation duration for binded pir type input.
 *
 * Notes:		
 *******************************************************************************************************************************/
uint8_t check_binded_pir_input_duration (uint8_t key) {
	uint8_t idx;
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		if (inps [idx].input_port_type == INPUT_PORT_TYPE_PIR) {
			if (inps [idx].binding_key == key) {
				if (inps [idx].pir_info->on_duration) {
					return PORT_STATE_ON;
				}
			}
		}
	}
	return PORT_STATE_OFF;
}

/********************************************************************************************************************************
 * Function name: 	uint8_t check_binded_switch_states_for_pir_duration (uint8_t key)
 *
 * Returns: 		uint8_t							Switch State i.e.:- PORT_STATE_ON/PORT_STATE_OFF
 *
 * Arguments: 		uint8_t key						Binding key of switch type input.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to check state of a binded switch for current pir type input.
 *
 * Notes:		
 *******************************************************************************************************************************/
uint8_t check_binded_switch_states_for_pir_duration (uint8_t key) {
	uint8_t idx;
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		if (inps [idx].input_port_type == INPUT_PORT_TYPE_ON_OFF) {
			if (inps [idx].binding_key == key) {
				if (inps [idx].curr_state == PORT_STATE_OFF) {
					return PORT_STATE_ON;
				}
			}
		}
	}
	return PORT_STATE_OFF;
}

/********************************************************************************************************************************
 * Function name: 	void allow_perms_fr_binded_fb_ip (uint8_t key)
 *
 * Returns: 		None
 *
 * Arguments: 		uint8_t key						Binding key of feedback type input.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to allow HVAC control permission to the system.
 *
 * Notes:			Thid routine is generally called when the switch is turned OFF.
 *******************************************************************************************************************************/
void allow_perms_fr_binded_fb_ip (uint8_t key) {
	uint8_t idx, perm;

	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		/* Checking all inputs for feedback type. */
		if (inps [idx].input_port_type == INPUT_PORT_TYPE_FEEDBACK) {
			if (inps [idx].binding_key == key) {
				perm = PERMISSION_ALLOWED;
				eeprom_data_read_write(FB_BSD_OP_CNTRL_PERM(idx), WRITE_OP, &perm, sizeof (perm));
			}
		}	
	}
}

/********************************************************************************************************************************
 * Function name: 	void execute_ip_based_ops (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to execute port operations on attached output ports according to valid input changes sensed.
 *
 * Notes:			PORT_STATE_ON  = 1
 *					PORT_STATE_OFF = 0
 *******************************************************************************************************************************/
void execute_ip_based_ops (void) {
	uint8_t idx;

	/* Check for all the input sensors */
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		/* Check if a valid input was sensed or not. */
		if (inps [idx].valid_input_sensed) {
			/* If change in states is sensed. */
			if (inps [idx].prev_state != inps [idx].curr_state) {
				/* Read the input port port type. */
				switch (inps [idx].input_port_type) {
					case INPUT_PORT_TYPE_PIR:
						if (inps [idx].curr_state == LOGIC_LEVEL_LOW) {
							/* Proceed only if system is in Automatic Mode. */
							if (sys_info.sys_mode == MODE_AUTOMATIC) {
								/* Turn the associated ouput ports ON if valid PIR input is sensed. */
								execute_op_ops (inps [idx].port_pool, PORT_STATE_ON);
								/* If binded switch is ON. */
								if (check_binded_switch_states_for_pir_duration (inps [idx].binding_key))  {
									inps [idx].pir_info->on_duration += MINS(30);
								}
							}
						}
						break;
					case INPUT_PORT_TYPE_ON_OFF:
						if (inps [idx].curr_state == LOGIC_LEVEL_LOW) {
							/* Turn the associated ouput ports ON if switch is turned ON. */
							execute_op_ops (inps [idx].port_pool, PORT_STATE_ON);
							/* Proceed only if system is in Automatic Mode. */
							if (sys_info.sys_mode == MODE_AUTOMATIC) {
								/* Increment ON duration of binded PIR inputs by 30 Mins.*/
								set_binded_pir_input_duration (inps [idx].binding_key, PORT_STATE_ON);
							}
						}
						else {
							/* Allow Permissions For Feedback Based Output Control on Binded Feedback Input. */
							allow_perms_fr_binded_fb_ip (inps [idx].binding_key);
							/* Turn the associated ouput ports OFF if switch is turned OFF. */
							execute_op_ops (inps [idx].port_pool, PORT_STATE_OFF);
							/* Set ON duration of binded PIR inputs to 0.*/
							set_binded_pir_input_duration (inps [idx].binding_key, PORT_STATE_OFF);
						}
						break;
					case INPUT_PORT_TYPE_FEEDBACK:
						if (inps [idx].curr_state == LOGIC_LEVEL_HIGH) {
							uint8_t perm;
							/* If Feedback is indicating that the device just got OFF. */
							if (check_binded_pir_input_duration (inps [idx].binding_key))  {
								/* If Pir Duration Has Not Expired, then save feedback based output control permission as Denied. */
								perm = PERMISSION_DENIED;
								eeprom_data_read_write(FB_BSD_OP_CNTRL_PERM(idx), WRITE_OP, &perm, sizeof (perm));
							}
//							else {
//								/* If Pir Duration Has Expired, then save feedback based output control permission as Allowed. */
//								perm = PERMISSION_ALLOWED;
//							}
//							eeprom_data_read_write(FB_BSD_OP_CNTRL_PERM(idx), WRITE_OP, &perm, sizeof (perm));
						}
						break;
					case INPUT_PORT_TYPE_MODE_SELECT:
						if (inps [idx].curr_state == LOGIC_LEVEL_HIGH) {
							sys_info.sys_mode = MODE_AUTOMATIC;
						}else {
							sys_info.sys_mode = MODE_MANUAL;
						}
						break;
				}
				/* Update Previous & Current States. */
				inps [idx].prev_state = inps [idx].curr_state;
			}
			/* Clear Valid Input Sensed Flag. */
			inps [idx].valid_input_sensed = 0;
		}
	}
}

/********************************************************************************************************************************
 * Function name: 	void init_ip_engine (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to initialize the complete input control engine.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void init_ip_engine (void) {
	initialize_port_mapping ();
	init_system_mode ();
	init_pir_subsystem ();
}

/********************************************************************************************************************************
 * Function name: 	void de_init_ip_engine (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to de-initialize the complete input control engine.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void de_init_ip_engine (void) {
	uint8_t idx;
	free_port_params ();
	free_pir_params ();
	free_attached_port_pools ();

	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		inps [idx].ports_active = 0;
		inps [idx].binding_key = 0;
		inps [idx].input_port_type = INPUT_PORT_TYPE_DORMANT;
	}
}


