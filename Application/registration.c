#include <stdint.h>
#include <stdlib.h>
#include "stm32f0xx_flash.h"
#include "stm32f0xx_iwdg.h"
#include "registration.h"
#include "timer.h"
#include "packet.h"
#include "uart.h"
#include "port_mapping.h"
#include "memory_info.h"
#include "pin_config.h"
#include "system.h"

#include "std_periph_headers.h"

extern struct system_params sys_info;
extern struct led_params led_info;

extern uint8_t uart_tx_flag;

/**************************************************************************************************
*				__task void tsk_UC_registration(void)
*
* @brief		- Waits for token, if received sends registration request after a random
*				  interval that can be 5x50 = 250ms.
*				- After sending the registration request ,waits for 2x50 = 100ms for
*				  HC to respond. It keeps validating packets and if the registration response is
*				  received then uUC extracts it's slave address, saves it on EEPROM and proceeds
*				  to the super loop.
*
* @author		Sahil Saini
* @date			5/8/13
* @note
**************************************************************************************************/
void set_rndm_dly_fr_reg_pkt_tx (void){
	uint8_t valid_random_delay_achieved = 0, random_delay;
	
	while(!valid_random_delay_achieved){
		random_delay = rand();
		if(random_delay <= 10){
			valid_random_delay_achieved = 1;
		}
	}
	random_delay *= 50;
	
	sys_info.snd_reg_pkt_flg = 1;
	timer_init(TIM3, TIME_MILLI_SEC, random_delay + 50);
	TIM_Cmd(TIM3, ENABLE);
	while (sys_info.snd_reg_pkt_flg) {
		/* Reload IWDG counter */
		IWDG_ReloadCounter();  
	}
//	toggle_com_led ();
	create_eco_wave_registration_pkt ();
}

void ChkIfSlaveRegistered (void) {
	if (sys_info.slave_add == BROADCAST_SLAVE_ADDRESS) {
		if (sys_info.brdcst_tkn_rcvd) {
			if (!sys_info.snd_reg_pkt_flg) {
				set_rndm_dly_fr_reg_pkt_tx ();
			}
			sys_info.brdcst_tkn_rcvd = 0;
		}
	}
}

void send_reg_pkt (serialport *usart, struct local_packet *out_frame_struct_ptr) {
	uint8_t temp_var, calc_crc_val;
	
	calc_crc_val = calc_crc4(out_frame_struct_ptr, 17);
	temp_var = PACKET_START | calc_crc_val;
	out_frame_struct_ptr -> start_byte = temp_var;
	out_frame_struct_ptr -> prio_stus_cmnd_byte = 0x00;
//	while(usart->usart_state == USART_TRANSMITTING);
//	usart->usart_state = USART_FILLING;
	
//	if(uart_tx_flag)																								// BY PARVESH
	 uart_send_str (USART1, (uint8_t *)out_frame_struct_ptr, 18);
	
//	usart->usart_state = USART_IDLE;

}

/************************************************************************//**
*				void create_uc_registration_pkt()
*
* @brief		Create Registration Packet with UID assigned.
*
* @param		None.
*
* @returns		None.
*
* @exception	None.
*
* @author		Sahil Saini
* @date			5/8/13
*
* @note
****************************************************************************/
void create_eco_wave_registration_pkt(){
	struct local_packet	out_frame_obj;
	uint8_t counter, data_byte_cnt;

	out_frame_obj.slave_add = 0;
	out_frame_obj.port_no = REGISTRATION_PN;
	out_frame_obj.frame_byte = 0x70;
	out_frame_obj.prio_stus_cmnd_byte = 0x00;
	
	counter = 24;
	for(data_byte_cnt = 0; data_byte_cnt < 4; data_byte_cnt++){
	   out_frame_obj.data_byte[data_byte_cnt] = (uint8_t)(*((uint32_t*)UIDMSWORDADD)>>counter) & 0xFF;
	   counter -= 8 ;
	}	
	counter = 24;
	for(data_byte_cnt = 4; data_byte_cnt < 8; data_byte_cnt++){
	   out_frame_obj.data_byte[data_byte_cnt] = (uint8_t)(*((uint32_t*)UIDMDWORDADD)>>counter) & 0xFF;
	   counter -= 8 ;
	}
	counter = 24;
	for(data_byte_cnt = 8; data_byte_cnt < 12; data_byte_cnt++){
	   out_frame_obj.data_byte[data_byte_cnt] = (uint8_t)(*((uint32_t*)UIDLSWORDADD)>>counter) & 0xFF;
	   counter -= 8 ;
	}			
	
	out_frame_obj.data_byte[12] = 0x00;

	send_reg_pkt(&usart1, &out_frame_obj);
}

uint8_t validate_uid_rcvd (uint8_t *ptr) {
	uint8_t counter, idx;
	uint32_t uidRcvd = 0, tRcvd = 0;

	counter = 24;
	for(idx = 0; idx < 4; idx++){
		tRcvd = *ptr++;
		uidRcvd |= (tRcvd<<counter); 
		tRcvd = 0;
		counter -= 8 ;
	}
	if (uidRcvd == *((uint32_t*)UIDMSWORDADD)) {
		uidRcvd = 0;
		counter = 24;
		for(idx = 0; idx < 4; idx++){
			tRcvd = *ptr++;
			uidRcvd |= (tRcvd<<counter); 
			tRcvd = 0;
			counter -= 8 ;
		}
		if (uidRcvd == *((uint32_t*)UIDMDWORDADD)) {
			uidRcvd = 0;
			counter = 24;
			for(idx = 0; idx < 4; idx++){
				tRcvd = *ptr++;
				uidRcvd |= (tRcvd<<counter); 
				tRcvd = 0;
				counter -= 8 ;
			}
		}
		if (uidRcvd == *((uint32_t*)UIDLSWORDADD)) {
			return 1;
		}
	}
	return 0;
}

