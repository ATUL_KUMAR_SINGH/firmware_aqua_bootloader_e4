/************************************************************************//**
* @file			profile.c
*
* @brief		This module contains the Profile Functionality in Cool Master Application.
*
* @attention	Copyright 2015 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 23/12/15 \n by \Sonam
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			 
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/
#include "profile.h"
#include "eeprom.h"

device_profile profile;

/************************************************************************//**
*					void profile_trigger(uint8_t profile_no)
*
* @brief			This routine is used to trigger specific profile number. 
*
* @param profile_no				Profile number for memory map address.
*
* @returns			
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
void profile_trigger(uint8_t profile_no){
 	uint32_t memory_address;
	uint8_t read_buff[20];
	
	memory_address = profile_no * 100;
	eeprom_data_read_write(memory_address, READ_OP, read_buff, 10);	
}

/************************************************************************//**
*					void profile_write(uint8_t profile_no, device_profile *PTRprofiles)
*
* @brief			This routine is used to write profile data on to memory as according to defined memory map.
*
* @param profile_no				Profile number for memory map address.
* @param *device_profile		Pointer to the structure that contains data.
*
* @returns			void.
*
* @exception		None.
*
* @author			
* @date				
* @note				None.                                       
****************************************************************************/
void profile_write(uint8_t profile_no, device_profile *PTRprofiles){
 	uint32_t memory_address;
	uint8_t read_buff[20];
	
	memory_address = profile_no * 100;
	eeprom_data_read_write(memory_address, WRITE_OP, read_buff, 10);	
}





