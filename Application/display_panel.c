/********************************************************************************************************************************
 * File name:	  	automated_task.c
 *
 * Attention:		Copyright 2014 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 13 May, 2014 by Nikhil Kukreja
 *
 * Description: 	This module handles all the automated task.
 *******************************************************************************************************************************/

/********************************************************************************************************************************
 * Include Section
 *******************************************************************************************************************************/
#include "water_management.h"
#include "stdint.h"
//#include "display_panel.h"
//#include "led.h"
#include "led.h"
#include "uart.h"
//#include "stm32f2xx_rcc.h"
//#include "stm32f2xx_gpio.h"

#include "buzzer.h"
#include "target.h"



 /*
**===========================================================================
**		Defines section
**===========================================================================
*/
//extern struct tank_info OHT_tank, UGT_tank;
volatile uint8_t oh_led_state_machine = 0, uh_led_state_machine = 0;
//extern struct pump_info  oht_pump_config, ugt_pump_config;
extern struct tank_status status;	
uint32_t oamp_led_state_machine;
#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	//
//	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif
//extern struct pump_info OHT_pump, UGT_pump;


/* A 2 D array holds the required pattern for each case, first element in each array hold total led views
in a complete cycle */
uint8_t led_patterns[20][10] = {{8, SHORT_RED, SHORT_OFF, SHORT_RED, SHORT_OFF, SHORT_RED, SHORT_OFF, SHORT_RED, SHORT_OFF},
								{4, LONG_RED, LONG_OFF, LONG_RED, LONG_OFF},
								{8, SHORT_ORANGE, SHORT_OFF, SHORT_RED, SHORT_OFF, SHORT_ORANGE, SHORT_OFF, SHORT_RED, SHORT_OFF},
								{4, LONG_ORANGE, LONG_OFF, SHORT_RED, CYCADJ_OFF},
								{8, SHORT_ORANGE, SHORT_OFF, SHORT_ORANGE, SHORT_OFF, SHORT_ORANGE, SHORT_OFF, SHORT_ORANGE, SHORT_OFF},
								{4, LONG_ORANGE, LONG_OFF, LONG_ORANGE, LONG_OFF},
								{8, SHORT_GREEN, SHORT_OFF, SHORT_ORANGE, SHORT_OFF, SHORT_GREEN, SHORT_OFF, SHORT_ORANGE, SHORT_OFF},
								{4, LONG_GREEN, LONG_OFF, SHORT_ORANGE, CYCADJ_OFF},
								{8, SHORT_GREEN, SHORT_OFF, SHORT_GREEN, SHORT_OFF, SHORT_GREEN, SHORT_OFF, SHORT_GREEN, SHORT_OFF},
								{4, LONG_GREEN, LONG_OFF, LONG_GREEN, LONG_OFF},	
								
								{4, PULSE_RED, INTPULSE_OFF, PULSE_RED, CYCADJ_OFF},
								{4, LONG_RED, LONG_RED, LONG_RED, LONG_RED},
								{8, PULSE_ORANGE, INTPULSE_OFF, PULSE_RED, INTPULSE_OFF, PULSE_ORANGE, INTPULSE_OFF, PULSE_RED, CYCADJ_OFF},
								{8, PULSE_ORANGE, INTPULSE_OFF, PULSE_ORANGE, SHORT_OFF, PULSE_RED, INTPULSE_OFF, PULSE_RED, CYCADJ_OFF},
								{4, PULSE_ORANGE, INTPULSE_OFF, PULSE_ORANGE, CYCADJ_OFF},
								{4, LONG_ORANGE, LONG_ORANGE, LONG_ORANGE, LONG_ORANGE},
								{8, PULSE_GREEN, INTPULSE_OFF, PULSE_ORANGE, INTPULSE_OFF, PULSE_GREEN, INTPULSE_OFF, PULSE_ORANGE, CYCADJ_OFF},
								{8, PULSE_GREEN, INTPULSE_OFF, PULSE_GREEN, SHORT_OFF, PULSE_ORANGE, INTPULSE_OFF, PULSE_ORANGE, CYCADJ_OFF},
								{4, PULSE_GREEN, INTPULSE_OFF, PULSE_GREEN, CYCADJ_OFF},
								{4, LONG_GREEN, LONG_GREEN, LONG_GREEN, LONG_GREEN}															
								};

uint8_t oamp_led_patterns[5] = {4, LONG_RED,LONG_RED,LONG_OFF,LONG_OFF};



/* These variables should be saved to data flash & fetched upon system reinit */
/* These are values in milli-seconds */
uint16_t led_long_interval = 1000;
uint16_t led_short_interval = 500;
uint16_t led_pulse_interval = 250;
uint16_t led_interpulse_interval = 150;
uint16_t led_adjust_interval = 1500;



/********************************************************************************************************************************
 * Function name: 	void display_led_pattern(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	13 may,2014
 *
 * Date modified:	13 may,2014
 *
 * Description: 	This function handles all the automated tasks
 *
 * Notes:
 *********************************************************************************************************/

void switch_led_pattern (uint8_t pattern_index, uint8_t tank_type) {
	uint8_t led_color;
	
	/* get led color */
	if (tank_type == OHT_TANK)
		led_color = led_patterns[oh_led_state_machine][pattern_index]%10;		
	else
		led_color = led_patterns[uh_led_state_machine][pattern_index]%10;		
		
	switch (led_color) {

		case LED_PULSE_INTERVAL:
		case LED_OFF:
			if (tank_type == OHT_TANK){
				TURN_OHT_OFF;
			}
			else {
				TURN_UGT_OFF;
			}
		break;

		case LED_RED:
			if (tank_type == OHT_TANK) {
				TURN_OHT_RED;
			}			
			else {
				TURN_UGT_RED;
			}
		break;

		case LED_GREEN:
			if (tank_type == OHT_TANK) {
				TURN_OHT_GREEN;
			}
			else {
				TURN_UGT_GREEN;
			}
		break;

		case LED_ORANGE:
			if (tank_type == OHT_TANK) {
				TURN_OHT_ORANGE;
			}
			else {
				TURN_UGT_ORANGE;
			}

		break;

	}
}

/********************************************************************************************************************************
 * Function name: 	void display_led_pattern_oht (void) 
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		praveen rana
 *
 * Date created: 	13 may,2014
 *
 * Date modified:	13 may,2014
 *
 * Description: 	This function display the led pattern for oht.
 *
 * Notes:
 *********************************************************************************************************/

void display_led_pattern_oht (void) {
	
	static uint8_t oh_led_sm_previous = 21, pattern_idx = 0, max_pattern_var = 0;
	static uint16_t led_service_cntr = 0;
	static uint16_t ticks_to_wait;
	uint8_t idx; // *ptr = 0;
	uint16_t ticks_consumed_in_cycle, temp_ticks;

	if(status.oht_current_level != 0){
		oh_led_state_machine = (status.oht_current_level/ 10) - 1;
	}
	else
		oh_led_state_machine = 0;
	if(status.oht_pump_ptr->pump_state == 0)

		oh_led_state_machine += 10;	
	/* Identify change in pattern if any */
	if (oh_led_state_machine ^ oh_led_sm_previous) {
		oh_led_sm_previous = oh_led_state_machine;	
		/* we need to implement led views from index 1 to last for a required case, so initialize 
		pattern idx to 1 */
		pattern_idx = 1;
	 	/* get total led views in a cycle for the case */
		max_pattern_var = led_patterns[oh_led_state_machine][0]; //4 or 8
		/* reinit main service counter to zero */
		led_service_cntr = 0;
		/* reinit waiting number of ticks to 0 */
		ticks_to_wait = 0;
		/* Switch to 1st led view in the cycle */
		switch_led_pattern (1, OHT_TANK);
	}

	/* fill ticks_to_wait with number of ticks required */
	if(!ticks_to_wait) {
		
		/* CYCADJ_OFF for motor ON case requires system to wait for duration specified by variable 
		led_adjust_interval; same holds true for motor OFF case, but for those cases of motor OFF
		where CYCADJ_OFF is one of the pattern, remaining cycle time is variable & not fixed, in this case
		remaining time is given by (cycle period - total time consumed for the cycle). so these two conditions 
		are hadled seperately */
		
		if ((oh_led_state_machine <= 9) || ((oh_led_state_machine >= 10) && (led_patterns[oh_led_state_machine][pattern_idx] != CYCADJ_OFF))) {		

			/* get duration to wait in terms of corresponding macro */
			ticks_to_wait = led_patterns[oh_led_state_machine][pattern_idx]/10;	//it shud give waiting number of ticks
			ticks_to_wait *= 10;
			/* get ticks to wait in ticks_to_wait ; divide by 50 as fn is called every 50ms */
			switch (ticks_to_wait) {
				
				case LED_PULSE_INTERVAL:
					ticks_to_wait = (led_interpulse_interval/50);	
				break;

				case LED_PULSE:
					ticks_to_wait = (led_pulse_interval/50);
				break;
	
				case LED_SHORT:
					ticks_to_wait = (led_short_interval/50);	
				break;
	
				case LED_LONG:
					ticks_to_wait = (led_long_interval/50);	
				break;
	
				case LED_CYCADJ:
					ticks_to_wait = (led_adjust_interval/50);	
				break;

			}
		}
		else {
			ticks_consumed_in_cycle = 0;
			/* calculate total time consumed for the cycle */
			for (idx = 1; idx < max_pattern_var; idx++) {
				
				temp_ticks = led_patterns[oh_led_state_machine][pattern_idx]/10;
				temp_ticks *= 10;
				switch (temp_ticks) {
					case LED_PULSE:
						temp_ticks = (led_pulse_interval/50);	
					break;
		
					case LED_SHORT:
						temp_ticks = (led_short_interval/50);	
					break;
		
					case LED_LONG:
						temp_ticks = (led_long_interval/50);	
					break;
		
					case LED_CYCADJ:
						temp_ticks = (led_adjust_interval/50);	
					break;
				}
				ticks_consumed_in_cycle =+ temp_ticks;
			}			
			
			/* get remaining time */
			if (ticks_consumed_in_cycle*50 >= (4*led_long_interval)) {
				ticks_to_wait = 0; /* This shouldn't be true; it indicates long, short, pulse etc are
				wrongly configured */
			}
			else {
				ticks_to_wait = (4*led_long_interval)/50;
				ticks_to_wait -= ticks_consumed_in_cycle;
			}
		}
	}
	
	/* Increment led_service_counter; when equals to ticks to wait, increment pattern index & switch to
	new pattern */
	if (++led_service_cntr >= ticks_to_wait) {
		
		if (++pattern_idx > max_pattern_var) {
			pattern_idx = 1;
		}
		switch_led_pattern (pattern_idx, OHT_TANK);		
		/* reinit service counter & ticks_to_wait */
		led_service_cntr = 0;
		ticks_to_wait = 0;
	}
}
/********************************************************************************************************************************
 * Function name: 	void display_led_pattern_ugt (void) 
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		praveen rana
 *
 * Date created: 	13 may,2014
 *
 * Date modified:	13 may,2014
 *
 * Description: 	This function display the led pattern for ugt.
 *
 * Notes:
 *********************************************************************************************************/
void display_led_pattern_ugt (void) {
	
	static uint8_t uh_led_sm_previous = 21, pattern_idx = 0, max_pattern_var = 0;
	static uint16_t led_service_cntr = 0;
	static uint16_t ticks_to_wait;
	uint8_t idx;
	uint16_t ticks_consumed_in_cycle, temp_ticks;
	
	if(status.ugt_current_level != 0){
		uh_led_state_machine = (status.ugt_current_level/10) - 1;
	}
	else
		uh_led_state_machine = 0;	
		
	if(status.ugt_pump_ptr->pump_state == 0)
		uh_led_state_machine += 10;	
	/* Identify change in pattern if any */
	if (uh_led_state_machine ^ uh_led_sm_previous) {
		uh_led_sm_previous = uh_led_state_machine;	
		/* we need to implement led views from index 1 to last for a required case, so initialize 
		pattern idx to 1 */
		pattern_idx = 1;
	 	/* get total led views in a cycle for the case */
		max_pattern_var = led_patterns[uh_led_state_machine][0]; //4 or 8
		/* reinit main service counter to zero */
		led_service_cntr = 0;
		/* reinit waiting number of ticks to 0 */
		ticks_to_wait = 0;
		/* Switch to 1st led view in the cycle */
		switch_led_pattern (1, UGT_TANK);
	}

	/* fill ticks_to_wait with number of ticks required */
	if(!ticks_to_wait) {
		
		/* CYCADJ_OFF for motor ON case requires system to wait for duration specified by variable 
		led_adjust_interval; same holds true for motor OFF case, but for those cases of motor OFF
		where CYCADJ_OFF is one of the pattern, remaining cycle time is variable & not fixed, in this case
		remaining time is given by (cycle period - total time consumed for the cycle). so these two conditions 
		are hadled seperately */
		
		if ((uh_led_state_machine <= 9) || ((uh_led_state_machine >= 10) && (led_patterns[uh_led_state_machine][pattern_idx] != CYCADJ_OFF))) {		

			/* get duration to wait in terms of corresponding macro */
			ticks_to_wait = led_patterns[uh_led_state_machine][pattern_idx]/10;	//it shud give waiting number of ticks
			ticks_to_wait *= 10;
			/* get ticks to wait in ticks_to_wait ; divide by 50 as fn is called every 50ms */
			switch (ticks_to_wait) {
				
				case LED_PULSE_INTERVAL:
					ticks_to_wait = (led_interpulse_interval/50);	
				break;

				case LED_PULSE:
					ticks_to_wait = (led_pulse_interval/50);
				break;
	
				case LED_SHORT:
					ticks_to_wait = (led_short_interval/50);	
				break;
	
				case LED_LONG:
					ticks_to_wait = (led_long_interval/50);	
				break;
	
				case LED_CYCADJ:
					ticks_to_wait = (led_adjust_interval/50);	
				break;

			}
		}
		else {
			ticks_consumed_in_cycle = 0;
			/* calculate total time consumed for the cycle */
			for (idx = 1; idx < max_pattern_var; idx++) {
				
				temp_ticks = led_patterns[uh_led_state_machine][pattern_idx]/10;
				temp_ticks *= 10;
				switch (temp_ticks) {
					case LED_PULSE:
						temp_ticks = (led_pulse_interval/50);	
					break;
		
					case LED_SHORT:
						temp_ticks = (led_short_interval/50);	
					break;
		
					case LED_LONG:
						temp_ticks = (led_long_interval/50);	
					break;
		
					case LED_CYCADJ:
						temp_ticks = (led_adjust_interval/50);	
					break;
				}
				ticks_consumed_in_cycle =+ temp_ticks;
			}			
			
			/* get remaining time */
			if (ticks_consumed_in_cycle*50 >= (4*led_long_interval)) {
				ticks_to_wait = 0; /* This shouldn't be true; it indicates long, short, pulse etc are
				wrongly configured */
			}
			else {
				ticks_to_wait = (4*led_long_interval)/50;
				ticks_to_wait -= ticks_consumed_in_cycle;
			}
		}
	}
	
	/* Increment led_service_counter; when equals to ticks to wait, increment pattern index & switch to
	new pattern */
	if (++led_service_cntr >= ticks_to_wait) {
		
		if (++pattern_idx > max_pattern_var) {
			pattern_idx = 1;
		}
		switch_led_pattern (pattern_idx, UGT_TANK);		
		/* reinit service counter & ticks_to_wait */
		led_service_cntr = 0;
		ticks_to_wait = 0;
	}
}

/********************************************************************************************************************************
 * Function name: 	void display_led_pattern_oamp (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		praveen rana
 *
 * Date created: 	13 may,2014
 *
 * Date modified:	13 may,2014
 *
 * Description: 	This function display the led pattern for oamp.
 *
 * Notes:
 *********************************************************************************************************/
void display_led_pattern_oamp (void) { 
	
	static uint8_t pattern_idx = 0, max_pattern_var = 0;
	static uint32_t oamp_led_sm_previous = 21;
	static uint16_t led_service_cntr = 0;
	static uint16_t ticks_to_wait;
	uint8_t  led_color;
//	uint16_t temp_ticks;
	
	
	/* Identify change in pattern if any */
	if (oamp_led_state_machine ^ oamp_led_sm_previous) {
		oamp_led_sm_previous = oamp_led_state_machine;	
		
		if (oamp_led_state_machine == 0) {
			TURN_OAMP_OFF;//tun off led
		}
		else if (oamp_led_state_machine == 1){
			TURN_OAMP_ON;//turn on led
		}
		else {		
			/* we need to implement led views from index 1 to last for a required case, so initialize 
			pattern idx to 1 */
			pattern_idx = 1;
		 	/* get total led views in a cycle for the case */
			max_pattern_var = oamp_led_patterns[0]; //4 or 8
			/* reinit main service counter to zero */
			led_service_cntr = 0;
			/* reinit waiting number of ticks to 0 */
			ticks_to_wait = 0;
			/* Switch to 1st led view in the cycle */
			led_color = oamp_led_patterns[1]%10;
			switch (led_color) {

				case LED_PULSE_INTERVAL:
				case LED_OFF:				
					TURN_OAMP_OFF; //turn led off;				
				break;
		
				case LED_RED:
					TURN_OAMP_ON;  //turn led on
				break;
			}
		}
	}
	if (oamp_led_sm_previous > 1) {
		/* fill ticks_to_wait with number of ticks required */
		if(!ticks_to_wait) {
			/* get duration to wait in terms of corresponding macro */
			ticks_to_wait = oamp_led_patterns[pattern_idx]/10;	//it shud give waiting number of ticks
			ticks_to_wait *= 10;
			/* get ticks to wait in ticks_to_wait ; divide by 50 as fn is called every 50ms */
			switch (ticks_to_wait) {
				
				case LED_PULSE_INTERVAL:
					ticks_to_wait = (led_interpulse_interval/50);	
				break;
	
				case LED_PULSE:
					ticks_to_wait = (led_pulse_interval/50);
				break;
	
				case LED_SHORT:
					ticks_to_wait = (led_short_interval/50);	
				break;
	
				case LED_LONG:
					ticks_to_wait = (led_long_interval/50);	
				break;
	
				case LED_CYCADJ:
					ticks_to_wait = (led_adjust_interval/50);	
				break;
	
			}
		}
		
		/* Increment led_service_counter; when equals to ticks to wait, increment pattern index & switch to
		new pattern */
		if (++led_service_cntr >= ticks_to_wait) {
			
			if (++pattern_idx > max_pattern_var) {
				pattern_idx = 1;
			}
			led_color = oamp_led_patterns[pattern_idx]%10;
			switch (led_color) {
	
				case LED_PULSE_INTERVAL:
				case LED_OFF:				
					TURN_OAMP_OFF; //turn led off;				
				break;
		
				case LED_RED:
					TURN_OAMP_ON; //turn led on
				break;
			}	
			/* reinit service counter & ticks_to_wait */
			led_service_cntr = 0;
			ticks_to_wait = 0;
		}
	}			
}

/********************************************************************************************************************************
 * Function name: 	void led_init(void)
 *
 * Returns: 		None
 *
 * Arguments: 		uint8_t				
 *
 * Created by: 		Nikhil kukreja
 *
 * Date created: 	1 Apr, 2014
 *
 * Date modified:	None
 *
 * Description: 	This function is used to initialize the led's.
 *
 * Notes:
 *******************************************************************************************************************************/
//void led_init(void){
//    GPIO_InitTypeDef GPIOstruct;
//
//    RCC_AHB1PeriphClockCmd (LED_CLOCK_PORT, ENABLE);  
//	RCC_AHB1PeriphClockCmd (OAMP_LED_RCC, ENABLE);          
//
//    printCli("\n\n\rInitializing LED...");						 
//
//    GPIOstruct.GPIO_Pin  =  OHT_LED_RED; 
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_PuPd = GPIO_PuPd_DOWN;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//
//    GPIO_Init(LED_PORT, &(GPIOstruct));  
//
//    GPIOstruct.GPIO_Pin  = OHT_LED_GREEN;           
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_PuPd = GPIO_PuPd_DOWN;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//    GPIO_Init(LED_PORT, &(GPIOstruct));  
//
//    GPIOstruct.GPIO_Pin  = UGT_LED_RED; 
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_PuPd = GPIO_PuPd_DOWN;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//    GPIO_Init(LED_PORT, &(GPIOstruct));  
//
//    GPIOstruct.GPIO_Pin  = UGT_LED_GREEN;           
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_PuPd = GPIO_PuPd_DOWN;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//    GPIO_Init(LED_PORT, &(GPIOstruct));  
//
//    GPIOstruct.GPIO_Pin  = OAMP_LED_RED;             
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_PuPd = GPIO_PuPd_DOWN;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//    GPIO_Init(LED_PORT, &(GPIOstruct));  
//}

void oamp_led_state(uint8_t state){

	if(state == 1)
		TURN_OAMP_ON
	else
		TURN_OAMP_OFF
}

void led_toggle(uint8_t led_type, uint8_t count){
	uint8_t loc;
	uint32_t delay;

	if(led_type == OAMP){
		for(loc = 0; loc < count; loc++){
			TURN_OAMP_ON;
			for(delay = 0; delay < 500000;delay++);
			for(delay = 0; delay < 500000;delay++);
			for(delay = 0; delay < 500000;delay++);
			TURN_OAMP_OFF;	
			for(delay = 0; delay < 500000;delay++);
			for(delay = 0; delay < 500000;delay++);
			for(delay = 0; delay < 500000;delay++);
		}			
	}

}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/



