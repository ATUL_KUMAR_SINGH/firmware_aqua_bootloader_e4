/********************************************************************************************************************************
 * File name:	  	automated_task.c
 *
 * Attention:		Copyright 2014 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 13 May, 2014 by Nikhil Kukreja
 *
 * Description: 	This module handles all the automated task.
 *******************************************************************************************************************************/

/********************************************************************************************************************************
 * Include Section
 *******************************************************************************************************************************/
//#include "ethernet_packet.h"
#include "water_management.h"
//#include "AT45DB161D.h"
//#include "ethernet_packet.h"
#include "rtc.h"
#include "buzzer.h"
//#include "logs.h"
#include "automated_tasks.h"
#include "client_registration.h"
//#include"ethernet_packet.h"
#include "stdio.h"
#include "string.h"
//#include <RTL.h>
//#include "stm32f2xx_gpio.h"
#include "memory_map.h"
#include "eeprom.h"


 /*
**===========================================================================
**		Defines section
**===========================================================================
*/

struct auto_task auto_task_info;
//extern struct pump_info OHT_pump, UGT_pump;
//extern struct tank_info OHT_tank, UGT_tank;
extern struct pump_info  oht_pump_config, ugt_pump_config;
extern struct wms_packet gui_send;
extern struct loc_tank_trigger_flags trigger_state;
extern uint8_t chk_dry_run_oht_flag, chk_dry_run_ugt_flag, oht_time_bw_sensor[10],ugt_time_bw_sensor[8], error_flag_num;
extern	struct wms_sys_info wms_sys_config;
extern uint8_t json_send_arr[1000], *json_send_ptr, oht_pump_trigger_cause , ugt_pump_trigger_cause ;
//extern OS_MUT token;
extern uint32_t system_notification_info;
uint16_t auto_task_num_flag,dry_run_oht_counter, dry_run_ugt_counter;
uint8_t auto_sub_task_num_flag;
#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif

struct auto_task_state auto_task_pump_state[9];

/*
**===========================================================================
**		global structures description
**===========================================================================
*	auto_task_num_flag		- holds the value of current automated process.

*/


/********************************************************************************************************************************
 * Function name: 	void auto_tsk_process(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	13 may,2014
 *
 * Date modified:	13 may,2014
 *
 * Description: 	This function handles all the automated tasks
 *
 * Notes:
 *********************************************************************************************************/
 uint8_t auto_tsk_process(void){
 	uint8_t critical_level;

 	auto_task_num_flag = 0;	               
	auto_sub_task_num_flag = 0;			   

	/* process task 1*******************************/
	if(auto_task_info.tsk_enable_flag[0] == 1){      /* process task 1 */
		if(status.oht_pump_ptr->pump_state == 1 && chk_dry_run_oht_flag == 0){
			chk_dry_run_oht_flag = 1;
			dry_run_oht_counter = auto_task_info.tsk_1_preset_level; 	/* in seconds(15 min) */						
		}	
	}
	
//	 /* process task 2*******************************/
//   	if(auto_task_info.tsk_enable_flag[1] == 1){     
//		if(status.ugt_pump_ptr->pump_state == 1 && chk_dry_run_ugt_flag == 0 && wms_sys_config.ugt_pump_select == 1){
//			chk_dry_run_ugt_flag = 1;
//			dry_run_ugt_counter = auto_task_info.tsk_2_preset_level; 	/* in seconds */			
//		}	
//	}
	/************************************************/
		/* process task 3 *******************************/
	 if(auto_task_info.tsk_enable_flag[2] == 1){   
	 	auto_sub_task_num_flag = 1;


	 }
	 
	/************************************************/

	/* process task 4 *******************************/
	 if(auto_task_info.tsk_enable_flag[3] == 1){   
	 	if(status.ugt_current_level <= auto_task_info.tsk_4_preset_level && wms_sys_config.total_ugt > 0){
	 		auto_task_num_flag |= (1 << 4);
			auto_sub_task_num_flag = 1;	
		}
	 }
	/************************************************/

	/* process task 5 *******************************/
	 if(auto_task_info.tsk_enable_flag[4] == 1 && wms_sys_config.total_ugt > 0){     
		critical_level = (status.ugt_current_level + status.oht_current_level)/2;
		if(critical_level <= auto_task_info.tsk_5_preset_level){
			auto_task_num_flag |= (1 << 5);
			auto_sub_task_num_flag = 1;
		}
	}
	/************************************************/
	/* process task 6 *******************************/
	if(auto_task_info.tsk_enable_flag[5] == 1){     
		if(status.ugt_current_level >= auto_task_info.tsk_6_preset_level[0] && status.oht_current_level < auto_task_info.tsk_6_preset_level[1] && wms_sys_config.total_ugt > 0){
		//	if(status.oht_pump_ptr->pump_state == 0){
				auto_task_num_flag |= (1 << 6);
				auto_sub_task_num_flag = 1;
		//	}	
		}
	}
		/* process task 8 *******************************/
	if(auto_task_info.tsk_enable_flag[7] == 1){      /* process task 8 */ //[Pump Off when OHT Full]
		if(status.oht_current_level == 100  ){
			auto_task_num_flag |= (1 << 8);
			auto_sub_task_num_flag = 1;
		}	
	}
 	/*********************************                 ***************/

	/* process task 9 *******************************/
//	if(auto_task_info.tsk_enable_flag[8] == 1){    
//		if(status.ugt_current_level == 100 &&  wms_sys_config.total_ugt > 0){
//			auto_task_num_flag |= (1 << 9);
//			auto_sub_task_num_flag = 1;
//		}
//	}

	/* process task 7 *******************************/
//	if(auto_task_info.tsk_enable_flag[6] == 1 && wms_sys_config.total_ugt > 0){ 		
//		if(status.ugt_current_level >= auto_task_info.tsk_7_preset_level[1] && status.oht_current_level <= auto_task_info.tsk_7_preset_level[0]){	   
//		//	if(status.oht_pump_ptr->pump_state == 0 || vitual_status_oht == 0){
//				auto_task_num_flag |= (1 << 7);
//				auto_sub_task_num_flag = 1;
//		//	}
//		}
//		if(status.ugt_current_level <= auto_task_info.tsk_7_preset_level[3] && status.oht_current_level >= auto_task_info.tsk_7_preset_level[2]){
//			//if(status.oht_pump_ptr->pump_state == 1 || vitual_status_oht == 1){
//				auto_task_num_flag |= (1 << 7);
//				auto_sub_task_num_flag = 2;
//			//}													   \\Unit_Controller\source/automated_tasks.c\auto_task_pump_state
//		}
//		if(status.oht_current_level == 100){
//			//if(status.oht_pump_ptr->pump_state == 1 || vitual_status_oht == 1){
//				auto_task_num_flag |= (1 << 7);
//				auto_sub_task_num_flag = 2;
//				
//			//}		
//		}
//	}
	/************************************************/
	

 	/************************************************/

 	/************************************************/

	
 	/************************************************/
   	return auto_task_num_flag;
}


/********************************************************************************************************************************
 * Function name: 	automated_task_enable(struct gui_payload *wms_payload_info, uint8_t num_bytes)
 *
 * Description: 	This function enables the automated task number.
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	16 may,2014
 *
 * Date modified:	16 may,2014

 *
 * Notes:	   
 *********************************************************************************************************/
void automated_task_enable(struct json_struct *wms_payload_info, uint8_t num_bytes){
	
	uint8_t  task_num, total_length, temp_arr[10], loc = 0, sub_cmnd, count = 0;
	
	/* extract sub command */
	total_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[loc]);
	loc += 	(total_length + 2);					// extract " "
	sub_cmnd =  ascii_decimal(&temp_arr[0], total_length);	

	total_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[loc]);
	loc += 	(total_length + 2);					// extract " "
	task_num = ascii_decimal(&temp_arr[0], total_length);				  /* extract task number */
	
	total_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[loc]);
	loc += 	(total_length + 2);	
	auto_task_info.tsk_enable_flag[task_num - 1] = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
	
	if(sub_cmnd == 2){						/* extract task configuration field */
		total_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[loc]);
		loc += 	(total_length + 2);	
		switch(task_num){
			case 1:
				auto_task_info.tsk_1_preset_level = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
			break;
			
			case 2:
				auto_task_info.tsk_2_preset_level = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
			break;

			case 3:
			//	auto_task_info.tsk_3_preset_level = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
			break;
				
			case 4:
				auto_task_info.tsk_4_preset_level = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
			break;

			case 5:
				auto_task_info.tsk_5_preset_level = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
			break;
				
			case 7:
				auto_task_info.tsk_7_preset_level[0] = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
				for(count = 1; count < 4; count++){
					total_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[loc]);
					loc += 	(total_length + 2);	
					auto_task_info.tsk_7_preset_level[count] = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
				}
			break;
			
				
			case 6:
				auto_task_info.tsk_6_preset_level[0] = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
				for(count = 1; count < 2; count++){
					total_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[loc]);
					loc += 	(total_length + 2);	
					auto_task_info.tsk_6_preset_level[count] = ascii_decimal(&temp_arr[0], total_length);	/* extract task enable disable flag */	
				}
			break;
			
		}
	/* save these setting to memory */
		
	}
	//dflash_write_multiple_byte(WATER_USAGE_INFO_START_ADDR, (uint8_t *)&auto_task_info.tsk_enable_flag[0], WATER_USAGE_LEN);
	eeprom_data_read_write(WATER_USAGE_INFO_START_ADDR, WRITE_OP, (uint8_t *)&auto_task_info.tsk_enable_flag[0], WATER_USAGE_LEN );	
}

/********************************************************************************************************************************
 * Function name: 	void create_automated_tsk_response(struct json_struct *wms_payload_info, uint8_t num_bytes)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	13 may,2014
 *
 * Date modified:	13 may,2014
 *
 * Description: 	This function handles all the automated tasks
 *
 * Notes:
 *********************************************************************************************************/

//void create_automated_tsk_response(struct json_struct *wms_payload_info, uint8_t num_bytes){
//	uint8_t field_name_indx = 1, nob, loc = 0;
//
//	*(json_send_ptr++) = '{';
//
//	while(loc != num_bytes){
//		add_field_name(field_name_indx++);
//		nob = copy_json_field_val((uint8_t *)&wms_payload_info->data[loc]);
//		*(json_send_ptr++) = ',';
//		loc += nob;
//	}
//	json_send_ptr--;
//	*(json_send_ptr++) = '}';
//	*(json_send_ptr++) = '}';
//	*(json_send_ptr++) = '\0';
//}

/********************************************************************************************************************************
 * Function name: 	void get_automated_task_status()
 *
 * Description: 	This function gets the state of automated tasks.
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	16 may,2014
 *
 * Date modified:	16 may,2014

 *
 * Notes:
 *********************************************************************************************************/
void get_automated_task_status(uint8_t *param){

	uint8_t  task_num, total_length, temp_arr[10], sub_cmnd, loc = 0, field_name_indx = 1, loc1;
	
	/* extract sub command */
	total_length = extract_json_field_val(&temp_arr[0], param);
	loc += 	(total_length + 2);					// extract " "
	sub_cmnd =  ascii_decimal(&temp_arr[0], total_length);	
	if(sub_cmnd == 1){
		*(json_send_ptr++) = '"';
		for(loc = 0 ; loc < TOTAL_AUTOMATED_TASK; loc++){
			*(json_send_ptr++) = auto_task_info.tsk_enable_flag[loc] + 0x30;
		}
		*(json_send_ptr++) = '"';		
	}
	else{
		total_length = extract_json_field_val(&temp_arr[0], (param + loc));
		loc += 	(total_length + 2);					// extract " "
		task_num =  ascii_decimal(&temp_arr[0], total_length);
			*(json_send_ptr++) = '{';
			add_field_name(field_name_indx++);
			*(json_send_ptr++) = '"' ;
			*(json_send_ptr++) = auto_task_info.tsk_enable_flag[task_num -1] + 0x30;		/* add task status */
			*(json_send_ptr++) = '"' ;
			if(task_num != 3){
			*(json_send_ptr++) = ',' ;
			add_field_name(field_name_indx++);
		//	if(auto_task_info.tsk_enable_flag[task_num -1] == 1){
			switch(task_num){
		
					case 1:
						*(json_send_ptr++) = '"' ;
						loc = dec_ascii_arr(auto_task_info.tsk_1_preset_level, json_send_ptr);
						json_send_ptr += loc; 
						*(json_send_ptr++) = '"' ;
						*(json_send_ptr++) = '}';
												   
					break;
		
				case 2:
					*(json_send_ptr++) = '"' ;
					loc = dec_ascii_arr(auto_task_info.tsk_2_preset_level, json_send_ptr);
					json_send_ptr += loc; 
					*(json_send_ptr++) = '"' ;
					*(json_send_ptr++) = '}';
				break;
				
//				case 3:
//					*(json_send_ptr++) = '"' ;
//					loc = dec_ascii_arr(auto_task_info.tsk_3_preset_level, json_send_ptr);
//					json_send_ptr += loc; 
//					*(json_send_ptr++) = '"' ;
//					*(json_send_ptr++) = '}';
//				break;
	
				case 4:
					*(json_send_ptr++) = '"' ;
					loc = dec_ascii_arr(auto_task_info.tsk_4_preset_level, json_send_ptr);
					json_send_ptr += loc; 
					*(json_send_ptr++) = '"' ;
					*(json_send_ptr++) = '}';
				break;

				case 5:
					*(json_send_ptr++) = '"' ;
					loc = dec_ascii_arr(auto_task_info.tsk_5_preset_level, json_send_ptr);
					json_send_ptr += loc; 
					*(json_send_ptr++) = '"' ;
					*(json_send_ptr++) = '}';
				break;
				
				case 7:
					*(json_send_ptr++) = '"' ;
					loc = dec_ascii_arr(auto_task_info.tsk_7_preset_level[0], json_send_ptr);
					json_send_ptr += loc; 
					*(json_send_ptr++) = '"' ;
					*(json_send_ptr++) = ',' ;
					for(loc1 = 1; loc1 < 4; loc1++){
						add_field_name(field_name_indx++);
						*(json_send_ptr++) = '"' ;
						loc = dec_ascii_arr(auto_task_info.tsk_7_preset_level[loc1], json_send_ptr);
						json_send_ptr += loc; 
						*(json_send_ptr++) = '"' ;
						*(json_send_ptr++) = ',' ;
					}
					json_send_ptr--;
					*(json_send_ptr++) = '}';
				break;
			  		
				case 6:
						*(json_send_ptr++) = '"' ;
						loc = dec_ascii_arr(auto_task_info.tsk_6_preset_level[0], json_send_ptr);
						json_send_ptr += loc; 
						*(json_send_ptr++) = '"' ;
						*(json_send_ptr++) = ',' ;
						add_field_name(field_name_indx++);
						*(json_send_ptr++) = '"' ;
						loc = dec_ascii_arr(auto_task_info.tsk_6_preset_level[1], json_send_ptr);
						json_send_ptr += loc; 
						*(json_send_ptr++) = '"' ;
						*(json_send_ptr++) = '}';
				break;
			}	
		}
		else{
			*(json_send_ptr++) = '}';
		}	
	}
   	*(json_send_ptr++) = '}';
	*(json_send_ptr++) = '\0';

}

/********************************************************************************************************************************
 * Function name: 	uint8_t automated_task_process(uint8_t tank_num){
 *
 * Description: 	This function process the automated tasks.
 *
 * Arguments: 		None
 *
 * Returns: 		Notification number if automated task runs
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	16 may,2014
 *
 * Date modified:	16 may,2014

 *
 * Notes:
 *********************************************************************************************************/
uint8_t automated_task_process(void){
	uint16_t error_flag_num = 0, count;

	if(auto_tsk_process() == 1){
		if(auto_task_num_flag <= 9)
			error_flag_num = auto_task_num_flag + 7;
		for(count = 0; count < 9; count++){
			system_notification_info &= ~(1 << (count + 7));
		}
		system_notification_info |= (1 << auto_task_num_flag);
			
	}
	return error_flag_num;
}

/********************************************************************************************************************************
 * Function name: 	uint8_t chk_automatic_task_status(uint8_t tank_num, uint8_t state)
 *
 * Description: 	This function checks whether current system state match with any automatic task condition.
 *
 * Arguments: 		tank_num - tank number
 					state    - current pump state
 *
 * Returns: 		ret  - 0  on no automatic task found
 						   automatic task number - otherwise
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	18 december,2015
 *			 
 *
 * Notes:
 *********************************************************************************************************/
 uint8_t chk_automatic_task_status(uint8_t tank_num, uint8_t state){
 	uint8_t ret = 0, count; 
	
	if(tank_num >= 1 && tank_num <= 25){
		if(auto_task_num_flag == 0)
			ret = 0;	
		 else{
			for(count = 3; count < 9; count++){
				if(((auto_task_num_flag >> count) & 0x01)== 0x01){
					if(((auto_task_pump_state[count - 1].oht_pump != state ||  auto_task_pump_state[count - 1].oht_pump == NOT_DEFINE )&& auto_sub_task_num_flag == 1) || ((state == ON ||  auto_task_pump_state[count - 1].oht_pump == NOT_DEFINE )&& auto_sub_task_num_flag == 2)){
						ret = count;
					}
				}
		 	}
		}		
	}
	else if(tank_num >= 26 && tank_num <= 50){
		if(auto_task_num_flag == 0)
			ret = 0;	
		 else{
		//	for(count = 1; count < 10; count++){
				if(((auto_task_num_flag >> 9) & 0x01)== 0x01){
					if(((auto_task_pump_state[8].ugt_pump != state ||  auto_task_pump_state[8].ugt_pump == NOT_DEFINE )&& auto_sub_task_num_flag == 1)){
						ret = 9;
					}
		//		}
		 	}
		}		
	}
	return ret; 
}
/********************************************************************************************************************************
 * Function name: 	uint8_t scan_pump_status(uint8_t tank_num)
 *
 * Description: 	This function check the current pump state with automatic task pump state.
 *
 * Arguments: 		tank_num - tank number
 *
 * Returns: 		ret  - 0  on no automatic task found
 						   1 - when current pump state and automatic task pump state different
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	18 december,2015
 *			 
 *
 * Notes:
 *********************************************************************************************************/

uint8_t scan_pump_status(uint8_t tank_num){
 	uint8_t ret = 0, count; 

	if(tank_num >= 1 && tank_num <= 25){
		if(auto_task_num_flag == 0)
			ret = 0;	
		for(count = 1; count < 9; count++){
			if(((auto_task_num_flag >> count) & 0x01)== 0x01){
				if(((auto_task_pump_state[count - 1].oht_pump != status.oht_pump_ptr->pump_state) &&  auto_task_pump_state[count - 1].oht_pump != NOT_DEFINE && auto_sub_task_num_flag == 1)){
					ret = 1;								
				}
				else if(((auto_task_pump_state[count - 1].oht_pump == status.oht_pump_ptr->pump_state) &&  auto_task_pump_state[count - 1].oht_pump != NOT_DEFINE && auto_sub_task_num_flag == 2 && count == 7)){
					ret = 1;								
				}
			}
		}
	}
	else if(tank_num >= 26 && tank_num <= 50){
		if(auto_task_num_flag == 0)
			ret = 0;	
		 
		 else if(((auto_task_num_flag >> 9) & 0x01)== 0x01){
			if((auto_task_pump_state[8].ugt_pump != status.ugt_pump_ptr->pump_state)){
				ret = 1;
			}
		}		
	}	
	
	return ret;	

}
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
