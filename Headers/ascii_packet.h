/************************************************************************************************************************************************************
* @file			ascii_packet.h
*
* @brief		Header for ascii_packet.c.
*
* @attention	Copyright 2013 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @author		Sahil Saini
*
************************************************************************************************************************************************************/
#ifndef ASCII_PACKET_H
#define ASCII_PACKET_H

/************************************************************************************************************************************************************
 * Include Section
 ***********************************************************************************************************************************************************/
#include <stdint.h>

/************************************************************************************************************************************************************
 * Defines section
************************************************************************************************************************************************************/
#define	HC_SERVER_ID  							9000
#define	HC_BUS_ADDR								100

#define MOTOR_MANUAL_FAILED						0
#define	MOTOR_SCHEDULE_FAILED					1
#define UT_EMPTY_OTM_FAILED						2
#define MOTOR_GUI_FAILED						3
#define TANK_SENSOR_MALFUNCTION					4
#define MOTOR_DRY_RUN_OCCURED					5
#define MOTOR_ON_FEEDBACK						6
#define MOTOR_OFF_FEEDBACK						7
#define MOTOR_FAULT_FEEDBACK					8

#define FIRMWARE_DOWNLOAD_CMD					8003
#define SAVE_DATE_TIME_CMD						8102
#define SEND_DATE_TIME_CMD						8103
#define REGISTERATION_CMD						8200
#define REGISTRATION_STAT_CMD					8210
#define SAVE_SENSOR_NUM_CMD						8501
#define SEND_TANK_LEVEL_CMD						8502
#define SEND_TANK_CONSUMPTION_CMD				8503
#define SEND_MOTOR_STATUS_CMD					8504
#define SEND_TANK_CAL_STATUS_CMD				8506
#define SAVE_VOL_BTW_SENSOR_CMD					8507
#define SEND_TOTAL_TANK_VOL_CMD					8508
#define CLEAR_CALIBRATION_CMD					8509
#define CALIBRATION_CMD							8510
#define SAVE_CONTAINER_VOL_CMD					8511
#define SAVE_ITERATION_CMD						8512
#define SAVE_NW_SETTINGS_CMD					8513
#define INFLOW_RATE_CMD							8514
#define SEND_RTC_ALARM_CMD						8515
#define TOTAL_TANKS_CMD							8516
#define SAVE_RTC_ALARM_CMD						8517
#define GUI_MOTOR_CONTROL_CMD					8518
#define MOTOR_START_FAILED						8519
#define SAVE_ADVISORY_MODE_CMD					8520
#define SEND_ADVISORY_MODE_CMD					8521
#define SAVE_THRESHOLD_CMD						8522
#define SEND_THRESHOLD_CMD						8523
#define SEND_LOGS_CMD							8524
#define BUZZER_ACTIVATE_DEACTIVATE_CMD			8525
#define BUZZER_SNOOZE_KILL_CMD					8526
#define	SAVE_TOTAL_TANK_VOL_CMD					8529
#define LED_DEMO_CMD							8530
#define DRY_RUN_CONTROL_CMD						8531
#define PRESET_LEVEL_CMD						8532
#define AUTOMATED_TASK_CMD						8533
#define SEND_RECORD_LOGS_CMD					8534
#define SENSOR_HEIGHT_CMD						8535
#define REGISTRATION_LIST_CMD					8702
#define REMOVE_REGISTRATION_CMD					8703
#define REPLACE_REGISTRATION_CMD				8704

/************************************************************************************************************************************************************
 * Global Variable Declaration Section
 ***********************************************************************************************************************************************************/
typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
//	uint8_t socket_num;
	uint32_t Server_id;						//	id for HC 9000
	uint32_t Message_length;
	uint32_t Sequence_num;
	uint8_t Ack_nack;
	uint8_t crc;
	uint32_t rfu;
	uint32_t Bus_addr;
}gui_header;

typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
	uint8_t slave_addr;
	uint16_t command;						//	id for HC 9000
	uint8_t port_num;
	uint8_t  data[50];
}hc_uc_payload_struct;

typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
	uint16_t line_no;
	uint8_t start;
	uint8_t rec_len;
	uint16_t mem_addr;
	uint8_t rec_type;
	uint8_t app_data[32];
	uint8_t crc;
}bootloader;

typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
	uint16_t 	mis_seq_arr[100];
	uint8_t 	retry_arr[100];
	uint16_t 	prev_validate_seq_num;
	uint8_t 	total_mis;
}Socket_Info;

extern uint8_t hcToGuiResponse[250];
extern uint8_t device_reg_stat;
extern uint8_t calibration_Start;

/***********************************************************************************************************************************************************
 * Prototype Section
 ***********************************************************************************************************************************************************/
void GUI_packet_validates(void);
long ascii_dec(uint8_t nob);
void dec_ascii_byte(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob);
void sendUpdatedLevel(uint8_t tank);
uint8_t download_firmware(void);
void sendCalibrationStatus(uint8_t tank);
void create_packet(uint16_t message_len, uint16_t command);
void send_motor_state(uint8_t tank_no);
uint8_t calc_crc(uint8_t *buff, uint16_t nob);
uint8_t cmp_str(uint8_t *dest_buf, uint8_t *src_buf, uint8_t len);
//void save_level_change_log(uint8_t param1, uint8_t param2, uint8_t param3, uint8_t tank);
//void save_motor_log(uint8_t param, uint8_t tank);
void notification_Cmd(uint8_t tank, uint8_t reason);

#endif
