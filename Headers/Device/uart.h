#ifndef __UART_H__
#define __UART_H__

#include "stm32f0xx_usart.h"
#define BAUDRATE	9600

#define USART_IDLE			0
#define USART_FILLING		1
#define USART_TRANSMITTING	2

void uart_init (USART_TypeDef* USARTx, uint32_t baud);
void uart_send_str (USART_TypeDef* USARTx, uint8_t *data_ptr, uint32_t bytes);
void uart_send_string (USART_TypeDef* USARTx, uint8_t *data_ptr);
#define RX_BUF_LEN		1024

#define TRUE 1
#define FALSE 0


#define BUFSIZE	1029
struct str{
	uint8_t uart_rx_buf[BUFSIZE]; 
	uint8_t * uart_rx_get_ptr;
	uint8_t * uart_rx_put_ptr;
};

struct boot{
	uint32_t seq_no_rx;
	uint8_t pkt_len;
	uint32_t address;
	uint8_t rec_type;
	uint8_t data_buff[35];					
	uint32_t seq_no;
	uint8_t chksum;
	uint16_t inv_buf[30];
};
extern struct boot bl;


#define 	BOOTCODE_ADD					0x08000000


#ifdef E4
#define 	APP_WAIT_TIME					120							  //120 sec 
#else
#define 	APP_WAIT_TIME					60								//60 sec
#endif


#ifdef E4
#define   REPEATED_REQUEST_TIME_OUT    200       // 200 * 10 = 2000 milisec
#else
#define   REPEATED_REQUEST_TIME_OUT    100       // 100 * 10 = 1000 milisec
#endif



#define   BL_DATA_PKT_START(V)	(((V & 0xFF) == 0x7E)? TRUE : FALSE) 

#define   BL_HEX_PKT_START(V)		(((V & 0xFF) == 0x3A)? TRUE : FALSE) 

#define		DATA_PKT_END(V)			(((V & 0xFF) == 0x5E)? TRUE : FALSE) 

#define		BL_FILE_END(V)			(((V & 0xFF) == 0x23)? TRUE : FALSE)

#define		RX_BYTE(V)				((V << 4) & 0xF0) 

#define 	NEG_ONE					-1				/* Negative one value for RxGetPtr Value if not valid */

#define		NEXT_GETPTR_BYTE(V)		((V != uart.uart_rx_put_ptr)? * uart.uart_rx_get_ptr : NEG_ONE)


typedef struct{
	unsigned char		InBuf[RX_BUF_LEN];
	unsigned char		* RxPutPtr;
	unsigned char		* RxGetPtr;
//	unsigned char		OutBuf[ETH_TX_BUF_LEN];
//	unsigned char		* TxPutPtr;
//	unsigned char		* TxGetPtr;
}serial_port;

#endif
