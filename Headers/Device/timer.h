#ifndef __TIMER_H__
#define __TIMER_H__

#include <stdint.h>
#include "stm32f0xx_tim.h"

/************************************************************************//**
*@enum			timer_bit_interval
*
* @brief		It can take values pertaining Circular Fifo current size.
****************************************************************************/
typedef enum {
	HALFBIT = 0,						// Timer running for half bit
	FULLBIT = 1,						// Timer running for full bit
}timer_bit_interval;


void Tim5IntOnCompare(void);

void Tim5IntTimeBase(void);
void Tim2IntTimeBase(void);
void TIM_Interrupt_Config(TIM_TypeDef* TIMx);
void timer_init(TIM_TypeDef* TIMx, uint8_t timer_type, uint16_t timer_val);

void TimerConfig(timer_bit_interval bitTime);


#define 	TIMEOUT_TIMER_IN_MS				50

#define 	TIME_MICRO_SEC					0

#define 	TIME_MILLI_SEC					1

#define 	TIME_SEC						2

#define 	TIME_MINUTE						3

#define 	TIME_HOUR						4

#endif
