#ifndef __PIN_CONFIG_H__
#define __PIN_CONFIG_H__

#include "stm32f0xx_gpio.h"

#define CHK_DBNC			1
#define DNT_CHK_DBNC		0

#define LOGIC_LEVEL_LOW		0
#define LOGIC_LEVEL_HIGH	1

#define DI1_PORT	GPIOB
#define DI1_PIN		GPIO_Pin_4

#define DI2_PORT	GPIOB
#define DI2_PIN		GPIO_Pin_5

#define DI3_PORT	GPIOB
#define DI3_PIN		GPIO_Pin_6

#define DI4_PORT	GPIOB
#define DI4_PIN		GPIO_Pin_7


#define DO1_DRIVE_PORT			GPIOA
#define DO1_DRIVE_PIN			GPIO_Pin_4
#define DO1_ON					GPIO_SetBits(DO1_DRIVE_PORT, DO1_DRIVE_PIN)
#define DO1_OFF					GPIO_ResetBits(DO1_DRIVE_PORT, DO1_DRIVE_PIN)

#define DO2_DRIVE_PORT			GPIOA
#define DO2_DRIVE_PIN			GPIO_Pin_5
#define DO2_ON					GPIO_SetBits(DO2_DRIVE_PORT, DO2_DRIVE_PIN)
#define DO2_OFF					GPIO_ResetBits(DO2_DRIVE_PORT, DO2_DRIVE_PIN)

#define DO3_DRIVE_PORT			GPIOA
#define DO3_DRIVE_PIN			GPIO_Pin_6
#define DO3_ON					GPIO_SetBits(DO3_DRIVE_PORT, DO3_DRIVE_PIN)
#define DO3_OFF					GPIO_ResetBits(DO3_DRIVE_PORT, DO3_DRIVE_PIN)

#define DO4_DRIVE_PORT			GPIOA
#define DO4_DRIVE_PIN			GPIO_Pin_7
#define DO4_ON					GPIO_SetBits(DO4_DRIVE_PORT, DO4_DRIVE_PIN)
#define DO4_OFF					GPIO_ResetBits(DO4_DRIVE_PORT, DO4_DRIVE_PIN)

#define SYS_LED_DRIVE_PORT		GPIOB
#define SYS_LED_DRIVE_PIN		GPIO_Pin_0
#define SYS_LED_ON				GPIO_SetBits(SYS_LED_DRIVE_PORT, SYS_LED_DRIVE_PIN)
#define SYS_LED_OFF				GPIO_ResetBits(SYS_LED_DRIVE_PORT, SYS_LED_DRIVE_PIN)

#define COMM_LED_DRIVE_PORT		GPIOB
#define COMM_LED_DRIVE_PIN		GPIO_Pin_1
#define COMM_LED_ON				GPIO_SetBits(COMM_LED_DRIVE_PORT, COMM_LED_DRIVE_PIN);led_info.comm_led_state = 1
#define COMM_LED_OFF			GPIO_ResetBits(COMM_LED_DRIVE_PORT, COMM_LED_DRIVE_PIN);led_info.comm_led_state = 0

#define WP_DRIVE_PORT			GPIOB
#define WP_DRIVE_PIN			GPIO_Pin_5
#define WP_ON					GPIO_SetBits(WP_DRIVE_PORT, WP_DRIVE_PIN)
#define WP_OFF					GPIO_ResetBits(WP_DRIVE_PORT, WP_DRIVE_PIN)

#define DEN_DRIVE_PORT			GPIOA
#define DEN_DRIVE_PIN			GPIO_Pin_8
#define DEN_ON					GPIO_SetBits(DEN_DRIVE_PORT, DEN_DRIVE_PIN)
#define DEN_OFF					GPIO_ResetBits(DEN_DRIVE_PORT, DEN_DRIVE_PIN)

void init_do (void);
void init_di (void);
void set_output_pin (GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void pin_int_ctrl (uint8_t idx, uint8_t flag);
uint8_t read_input_pin (uint8_t pin_no);
void set_do (uint8_t outport, uint8_t level);
void toggle_sys_led (void);
void toggle_com_led (void);

#endif
