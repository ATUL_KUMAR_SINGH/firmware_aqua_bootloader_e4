#ifndef __TIME_BASED_OPS_H__
#define __TIME_BASED_OPS_H__

void serve_sampling_window_timer (void);
void serve_debounce_check_timer (void);

#endif
