/************************************************************************************************************************************************************
* @file			water_level_manaegment.h
*
* @brief		Header for water_level_manaegment.c.
*
* @attention	Copyright 2013 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @author		Sahil Saini
*
************************************************************************************************************************************************************/
#ifndef WATER_LEVEL_MANAGEMENT_H_
#define WATER_LEVEL_MANAGEMENT_H_

/************************************************************************************************************************************************************
 * Include Section
 ***********************************************************************************************************************************************************/
#include "bitops.h"

/************************************************************************************************************************************************************
 * Defines section
************************************************************************************************************************************************************/
///////////////////////////////////////ARRAY SIZE DEFINITIONS///////////////////////////////////////
#define MAX_TANKS								2
#define MAX_SENSORS_NUM							10
#define MAX_ALARMS								10
////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct __attribute__((__packed__)) {			// To avoid structure padding.
	uint8_t alarm_id;									/*	Used to store alarm id for display purposes on GUI.*/
	uint16_t alarm_minutes;								/*	Used to store alarm minutes for schedules for tank.*/
	uint8_t alarm_status;								/*	Used to store state of motor for current alarm.*/
	uint8_t tank_no;									/*	Used to store tank to which current alarm belongs.*/
}alarm_time;

typedef struct __attribute__((__packed__)) {			// To avoid structure padding.
	uint8_t		status_type_id;							/*	Used to store:-
															1). Tank Status (Bit 7 = 1 - Tank Active)
																			(Bit 7 = 0 - Tank Inactive)
															2). Tank Type	(Bit 4 = 1 - Underground Type)
																			(Bit 4 = 0 - Overhead Type)
															3). Tank ID		(Bit 0 - 3 = Tank Number)
														*/
	uint8_t		active_alarms;							/*	Used to store number of active alarms.*/
	uint8_t		vol_setting;							/*	Used to store volume setting type:-
	 	 	 	 	 	 	 	 	 	 	 	 	 		vol_setting = 0 - Only Volume.
	 	 	 	 	 	 	 	 	 	 	 	 	 		vol_setting = 1 - Only Length, Width & Height.
	 	 	 	 	 	 	 	 	 	 	 	 	 		vol_setting = 2 - Both Volume along with Length, Width & Height.
	 	 	 	 	 	 	 	 	 	 	 	 	 	*/
	uint8_t		calib_stat;								/*	Used to store calibration status of tank:-
	 	 	 	 	 	 	 	 	 	 	 	 	 		calib_stat = 0 - Calibration Not Done.
	 	 	 	 	 	 	 	 	 	 	 	 	 		calib_stat = 1 - Calibration Done.
	 	 	 	 	 	 	 	 	 	 	 	 	 		calib_stat = 2 - Calibration In Progress.
	 	 	 	 	 	 	 	 	 	 	 	 	 		calib_stat = 3 - Calibration Time Count Start.
	 	 	 	 	 	 	 	 	 	 	 	 	 	*/
	uint8_t 	vol_calib_stat;							/*	Used to store volume calibration status of tank:-
															vol_calib_stat = 0 - Volume Calibration Not Done.
															vol_calib_stat = 1 - Volume Calibration Done.
	 	 	 	 	 	 	 	 	 	 	 	 	 	*/
	uint8_t		sensors;								/*	Used to store number of sensors in a tank.*/
	uint8_t		lower_preset_level;						/*	Used to store sensor level below which motor needs to be turned ON if water falls below this level.*/
	uint8_t		motor_switch_port;						/*	Used to store motor switch's port number.*/
	uint8_t 	motor_relay_port;						/*	Used to store motor relay's port number.*/
	uint8_t		led_port1;								/*	Used to store red led's port number.*/
	uint8_t		led_port2;								/*	Used to store green led's port number.*/
	float 		total_vol;								/*	Used to store tank's total volume.*/
	float 		length;									/*	Used to store tank's length.*/
	float 		width;									/*	Used to store tank's width.*/
	float 		height;									/*	Used to store tank's height.*/
	alarm_time	*alarm_ptr;								/*	Used to point to the array buffer holding all the alarms for tanks.*/
	float		*cap_btw_sensor_ptr;					/*	Used to point to the array buffer holding all the capacities between sensors in terms of percentage for tanks.*/
	uint8_t		*sensor_port_ptr;						/*	Used to point to the array buffer holding all the sensor ports for tanks.*/
	uint32_t	*tim_btw_sensor_ptr;					/*	Used to point to the array buffer holding all the filling times between sensors in terms of seconds.*/
}tank_rom;

typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
	uint8_t 	led_state					:1;			/*	Used to indicate led states for the led. i.e ON/OFF.*/
	uint8_t 	led_blink_flag				:1;			/*	Used to indicate when led is to be blinked.*/
	uint8_t 	motor_state					:1;			/*	Used to indicate motor state. i.e. ON/OFF.*/
	uint8_t 	chk_dry_run_flag			:1;			/*	Used to check if dry run checking is to be performed for a tank or not.*/
	uint8_t		led_demo_mode_flag			:1;			/*	Used to indicate when leds are to be operated in demo mode.*/
	uint8_t		alarm_act					:1;			/*	Used to indicate alarm action. i.e. motor ON/OFF. */
	uint8_t 	led_clr_cur					:2;			/*	Used to store led's current color.*/
	uint8_t		led_demo_mode_level			:4;			/*	Used to store water level in demo mode.*/
	uint8_t		led_demo_mode_motor_state	:1;			/*	Used to store motor state in demo mode.*/
	uint8_t		led_mode					:3;			/*	Used to store led mode:-
	 	 	 	 	 	 	 	 	 	 	 	 	 		led_mode = L1/M1/F1 if motor ON.
	 	 	 	 	 	 	 	 	 	 	 	 	 		led_mode = L2/M2/F2 if motor ON.
	 	 	 	 	 	 	 	 	 	 	 	 	 		led_mode = L3/M3/F3 if motor ON.
	 	 	 	 	 	 	 	 	 	 	 	 	 		led_mode = L1/M1/F1 if motor OFF.
	 	 	 	 	 	 	 	 	 	 	 	 	 		led_mode = L2/M2/F2 if motor OFF.
	 	 	 	 	 	 	 	 	 	 	 	 	 		led_mode = L3/M3/F3 if motor OFF.
	 	 	 	 	 	 	 	 	 	 	 	 	 	*/
	uint8_t		level_changed_flag			:1;			/*	Used to indicate if level change has occurred or not.*/
	uint8_t		motor_switch_state			:1;			/*	Used to indicate if the motor switch is currently pressed or not.*/
	uint8_t		motor_switch_pressed_cntr;				/*	Used to count number of seconds for which motor button has been pressed.*/
	uint8_t		level_settling_cntr;					/*	Used to count number of seconds required for a sensor to settle down after an event.*/
	uint8_t		led_demo_cntr;							/*	Used to count number of seconds for which tank is in demo mode.*/
	uint8_t		led_interval;							/*	Used to keep the interval time between current and next led event.*/
	uint8_t		relay_feedback_state;					/*	Used to keep the current feedback state of relays to detect if any error condition occurs.*/
	uint8_t 	highest_set_sensor;						/*	Used to keep the highest sensor number that is set.*/
	uint8_t 	sensor_changed;							/*	Used to indicate which sensor has changed.*/
	uint16_t	alarm_min;								/*	Used to keep the next up-coming alarm time for current tank.*/
	uint32_t 	dry_run_counter;						/*	Used to maintain the count of seconds required by water to hit the next sensor when water is filling.*/
	uint32_t 	waterSensorIpValue;						/*	Used to maintain previous tank input states like sensor & motor button state.*/
	uint32_t 	waterSensorIpNewValue;					/*	Used to maintain current tank input states like sensor & motor button state.*/
	float 		cur_real_water_lvl;						/*	Used to keep current water level in tank.*/
	float 		prev_real_water_lvl;					/*	Used to keep previous water level in tank.*/
	float		inflow_rate;							/*	Used to keep in-flow rate of water for current tank.*/
	double 		virtual_vol_base;						/*	Used to keep the base volume/previous virtual volume.*/
	double		virtual_vol_to_be_sent;					/*	Used to keep the current virtual volume to be sent.*/
}tank_ram;

/*******************************************************Address Offsets*************************************/
#define VER_ADD									1
#define WIFLY_MODE_LOCK_ADD						2
#define KP_CONFIG_STAT_ADD						3
#define ROW_ADD									4
#define COL_ADD									5
#define NO_OF_KEYS_ADD							6
#define NEW_FIRMWARE_CHECK_ADD					7
#define NUMBER_OF_TANKS_ADD						8
#define NUMBER_OF_UT_ADD						9
#define NUMBER_OF_OT_ADD						10
#define PREDICTIVE_VOL_ADD						11
#define DRY_RUN_CTRL_ADD						12
#define TANK_AUTOMATED_TASK_FLAG_ADD			13
#define PREVIOUS_INDEX_CONSUMPTION_LOGGED_ADD	14

#define	REG_KEY_OFFSET							15
#define SYS_LOW_THRESHOLD_ADD					110
#define SYS_MED_THRESHOLD_ADD					111
#define SYS_HIGH_THRESHOLD_ADD					112
#define BUZZER_STATE_ADD						113
#define SYS_MODE_ADD							114
#define LOG_GET_ADD								115
#define LOG_PUT_ADD								116
#define ERROR_LOG_GET_ADD						117
#define ERROR_LOG_PUT_ADD						118
#define CALIBRATED_SENSOR_ADD					119

#define NETWORK_SETTING_SSID					120
#define NETWORK_SETTING_IP						140
#define NETWORK_SETTING_NET						156
#define NETWORK_SETTING_GATEWAY					172
#define NETWORK_SETTING_PHRASE					190

#define LEVEL_CHANGE_RECORD_LOG_GET_ADD			200
#define LEVEL_CHANGE_RECORD_LOG_PUT_ADD			201
#define MOTOR_LOG_GET_ADD						202
#define MOTOR_LOG_PUT_ADD						203

#define ADVISORY_LOGS_OFFSET					250

#define ERROR_LOGS_OFFSET						(uint16_t)400

#define TANK_INFO_START_ADD						(uint32_t)600
#define TANK_TYPE_ID_ADD						(0)
#define TANK_ACTIVATED_ALARMS_ADD				(TANK_TYPE_ID_ADD + 1)
#define TANK_VOLUME_SETTING_TYPE_ADD			(TANK_ACTIVATED_ALARMS_ADD + 1)
#define TANK_CALIBRATION_STATUS_ADD				(TANK_VOLUME_SETTING_TYPE_ADD + 1)
#define TANK_VOL_CALIB_STATUS_ADD				(TANK_CALIBRATION_STATUS_ADD + 1)
#define NUMBER_OF_SENSORS_IN_TANK_ADD			(TANK_VOL_CALIB_STATUS_ADD + 1)
#define TANK_LOWER_PRESET_LEVEL_ADD				(NUMBER_OF_SENSORS_IN_TANK_ADD + 1)
#define	MOTOR_SWITCH_PORT						(TANK_LOWER_PRESET_LEVEL_ADD + 1)
#define MOTOR_RELAY_PORT_ADD					(MOTOR_SWITCH_PORT + 1)
//#define TANK_LED1_PORT_ADD						(MOTOR_RELAY_PORT_ADD + 1)
//#define TANK_LED2_PORT_ADD						(TANK_LED1_PORT_ADD + 1)
//#define TANK_TOTAL_VOL_ADD						(TANK_LED2_PORT_ADD + 1)
#define TANK_TOTAL_VOL_ADD						(MOTOR_RELAY_PORT_ADD + 1)
#define TANK_LENGTH_ADD							(TANK_TOTAL_VOL_ADD + 4)
#define TANK_WIDTH_ADD							(TANK_LENGTH_ADD + 4)
#define TANK_HEIGHT_ADD							(TANK_WIDTH_ADD + 4)
#define TANK_ALARMS_OFFSET						(TANK_HEIGHT_ADD + 4)
#define TANK_CAPACITY_BTW_SENSORS_OFFSET		(TANK_ALARMS_OFFSET + (5 * MAX_ALARMS))
#define TANK_SENSOR_PLACEMENT_OFFSET			(TANK_CAPACITY_BTW_SENSORS_OFFSET + (MAX_SENSORS_NUM * 4))
#define TANK_FILL_TIME_BTW_SENSORS_OFFSET		(TANK_SENSOR_PLACEMENT_OFFSET + MAX_SENSORS_NUM)
#define TANK_INFO_SIZE						    ((TANK_FILL_TIME_BTW_SENSORS_OFFSET + (MAX_SENSORS_NUM * 4)) - TANK_TYPE_ID_ADD)
#define TANK_INFO_ADD(x)						(TANK_INFO_START_ADD + (x*TANK_INFO_SIZE))


#define TANK_EXTRA_INFO_START_ADD				TANK_INFO_ADD(2)

#define MOTOR_ON_TIME_OFFSET					0
#define MOTOR_OFF_TIME_OFFSET					(MOTOR_ON_TIME_OFFSET + 5)

#define LEVEL_CHANGE_RECORD_OFFSET				(MOTOR_OFF_TIME_OFFSET + 5)
#define MOTOR_LOG_OFFSET						(LEVEL_CHANGE_RECORD_OFFSET + (LEVEL_CHANGE_RECORD_LEN * TOTAL_LOGS))

#define EQUIDISTANT_SENSOR_FLAG_ADD				(MOTOR_LOG_OFFSET + (MOTOR_STATUS_RECORD_LEN * TOTAL_LOGS))
#define FLOW_RATE_SETTING_ADD					(EQUIDISTANT_SENSOR_FLAG_ADD + 1)
#define DISCHARGE_CAP_ADD						(FLOW_RATE_SETTING_ADD + 1)
#define POWER_ADD								(DISCHARGE_CAP_ADD + 4)
#define PIPE_HEIGHT_ADD							(POWER_ADD + 4)
#define PIPE_DIA_ADD							(PIPE_HEIGHT_ADD + 4)
#define SENSOR_HEIGHT_OFFSET					(PIPE_DIA_ADD + 4)
#define SENSOR_HEIGHT_FOR_SENSOR(x)				(SENSOR_HEIGHT_OFFSET + (x*2))
#define TANK_CONSUMPTION_OFFSET					SENSOR_HEIGHT_FOR_SENSOR(MAX_SENSORS_NUM)				//(SENSOR_HEIGHT_ADD + 4)
#define CONSUMPTION_DAY_1_OFFSET				TANK_CONSUMPTION_OFFSET
#define CONSUMPTION_DAY_OFFSET(day)				(CONSUMPTION_DAY_1_OFFSET + ((day-1)*96))
#define TANK_INFO_END_OFFSET					(CONSUMPTION_DAY_OFFSET(31))
#define TANK_EXTRA_INFO_SIZE				    (TANK_INFO_END_OFFSET - MOTOR_ON_TIME_OFFSET)

#define TANK_EXTRA_INFO_ADD(x)					(TANK_EXTRA_INFO_START_ADD + (x*TANK_EXTRA_INFO_SIZE))

#define T_WATER_CONSUME_DAY_ADD(tank, day)		((TANK_EXTRA_INFO_ADD(tank) + TANK_CONSUMPTION_OFFSET) + (96*(day-1)))

#define FIRMWARE_AVAILABLE_STATUS				(uint16_t)8200
#define	NEW_FIRMWARE_AVAILABLE_STATUS			(uint16_t)8201
#define	BOOT_JUMP_STATUS						(uint16_t)8202
#define BOOTLOADER_START_ADD					(uint16_t)8203
#define	APP_CODE_START_ADD						(uint16_t)8205
#define	BOOT_INV_ADD_OFFSET						(uint16_t)8210
#define APP_INV_ADD_OFFSET						(uint16_t)8260
#define APP_CODE_OFFSET							(uint16_t)8310




//#define T_WATER_CONSUME_DAY_ADD(tank, day)		(TANK_EXTRA_INFO_ADD(tank) + TANK_CONSUMPTION_OFFSET + (uint16_t )(96*((uint16_t)day-1)))		// 4 BYTE EACH HR

#define BASE_BOARD								1
#define EXP_BOARD								2

#define MOTOR_ON								1
#define MOTOR_OFF								0

#define TANK_1									1
#define TANK_2									2
#define TANK_3									3

#define OVERHEAD								0
#define UNDERGROUND								1

#define TANK_ACTIVE								1
#define TANK_DEACTIVE							0

#define CHECK_TANK_TYPE(V)						Is_Set(V,4)
#define IS_TANK_ACTIVE(V)						Is_Set(V,7)

#define ACTIVATE_TANK(V)						Set_Bits(V,7)
#define DEACTIVATE_TANK(V)						Clear_Bits(V,7)

#define SOFT_RESTART_TIME						30
#define PARTIAL_RESET_RESTART_TIME				60
#define FULL_RESET_RESTART_TIME					120

/************************************************************************************************************************************************************
 * Global Variable Declaration Section
 ***********************************************************************************************************************************************************/
extern uint8_t total_number_of_tanks, total_UT, total_OT;
extern uint8_t waterFillTimeCount/*, container_iterations*/;
extern uint8_t tank_for_which_calibration_active;
extern uint8_t sys_low_th, sys_med_th, sys_high_th;
extern uint8_t predictive_vol_flag, check_automated_task;
extern uint8_t sys_mode, send_timed_vol_flag, check_dry_run_flag;
extern tank_rom tank_rom_obj[MAX_TANKS];
extern tank_ram tank_ram_obj[MAX_TANKS];

/***********************************************************************************************************************************************************
 * Prototype Section
 ***********************************************************************************************************************************************************/
void check_active_calibration(void);

void set_defaults(void);

void waterLevelMgtInit(void);

//void clearCalibraton(uint8_t tank);

void clearWaterConsumption(void);

void wlms_input_op(void);

uint8_t getSensorReading(uint8_t sensor);

void actuateMotor(uint8_t motor,uint8_t swtch);

void water_level_measurement(uint8_t tank);

void sort_alarm_buffer(alarm_time *tank_ptr, uint8_t no_of_alarms);

void configure_alarm(uint8_t tankNum);

void save_Alarms(uint8_t tank);

void check_scheduled_event_for_tank(uint8_t tank);

void send_predictive_volume_for_tank(uint8_t tank);

void check_water_sensors(void);

#endif

