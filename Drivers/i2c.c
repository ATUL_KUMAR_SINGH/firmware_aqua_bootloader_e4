/**
  ******************************************************************************
  * @file    stm320518_eval.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    20-April-2012
  * @brief   This file provides set of firmware functions to manage Leds, 
  *          push-button and COM ports.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_i2c.h"
#include "stm32f0xx_gpio.h"
#include "i2c.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_misc.h"
#include "eeprom.h"

#include "std_periph_headers.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
i2c_states i2c_state = I2C_IDLE;
i2c_ops i2c_op = I2C_WRITE; 

uint8_t i2c_slave_addr;
uint8_t *i2c_mem_addr_ptr;
uint8_t *i2c_data_ptr;
uint8_t i2c_num_of_bytes;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  This function handles I2C1 Error interrupt request.
  * @param  None
  * @retval None
  */
void I2C1_IRQHandler(void) {
	static uint8_t idx = 2;
	uint32_t status_flag = I2C_ReadRegister(I2C1, I2C_Register_ISR) & 0x0FFE;
	
	switch (status_flag) {
		case I2C_IT_TXIS:
			switch (i2c_state) {
				case I2C_TRANSACT_START:
					I2C_SendData(I2C1, *(i2c_mem_addr_ptr + idx - 1));
					if (--idx == 0) {
						idx = 2;
						i2c_state = I2C_MEM_ADD_SENT;
						if (i2c_op == I2C_READ) {
							I2C_MasterRequestConfig (I2C1, I2C_Direction_Receiver);
							I2C_TransferHandling (I2C1, (i2c_slave_addr|0x01), i2c_num_of_bytes, I2C_SoftEnd_Mode, I2C_Generate_Start_Read);
						}
					}					
					break;
				case I2C_MEM_ADD_SENT:
					if (i2c_op == I2C_WRITE) {
						I2C_SendData(I2C1, *i2c_data_ptr++);
						i2c_num_of_bytes--;
					}
					break;
				case I2C_TRANSACT_COMPLETE:
					break;
			}
			break;
		case I2C_IT_RXNE:
			switch (i2c_state) {
				case I2C_MEM_ADD_SENT:
					if (i2c_op == I2C_READ) {
						*(i2c_data_ptr++) = I2C_ReceiveData(I2C1);
					}
 					break;
			}
			break;
		case I2C_IT_TC:
			i2c_state = I2C_TRANSACT_COMPLETE;
			I2C_GenerateSTOP(I2C1, ENABLE);
			break;
		default:
			I2C_ClearITPendingBit(I2C1, status_flag);
			break;
	}
}

/**
  * @brief  Initializes peripherals used by the I2C EEPROM driver.
  * @param  None
  * @retval None
  */
void i2c_init (void)
{
	GPIO_InitTypeDef GPIO_InitStructure; 
	I2C_InitTypeDef  I2C_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
		RCC_APB1PeriphClockCmd(EE_I2C_CLK, ENABLE);
 
    /* Configure the I2C clock source. The clock is derived from the HSI */
    RCC_I2CCLKConfig(RCC_I2C1CLK_HSI);
//	  RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);
 
    GPIO_PinAFConfig(EE_I2C_SCL_GPIO_PORT, EE_I2C_SCL_SOURCE, EE_I2C_SCL_AF);
    GPIO_PinAFConfig(EE_I2C_SDA_GPIO_PORT, EE_I2C_SDA_SOURCE, EE_I2C_SDA_AF);
 
 
    //Configure pins: SCL and SDA ------------------
    GPIO_InitStructure.GPIO_Pin = EE_I2C_SCL_PIN | EE_I2C_SDA_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
 
    I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
    I2C_InitStructure.I2C_DigitalFilter = 0x00;
    I2C_InitStructure.I2C_OwnAddress1 = 0x00; // MPU6050 7-bit adress = 0x68, 8-bit adress = 0xD0;
    I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStructure.I2C_Timing = 0xB0421214;
    I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
 
    I2C_Init(EE_I2C, &I2C_InitStructure);
 
    I2C_Cmd(EE_I2C, ENABLE);

	/* Enable Error Interrupt */
	I2C_ITConfig (EE_I2C, I2C_IT_TCI | I2C_IT_RXI | I2C_IT_TXI , ENABLE);

	/* Reconfigure and enable I2C1 error interrupt to have the higher priority */
	NVIC_InitStructure.NVIC_IRQChannel = I2C1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
