#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_syscfg.h"
#include "pin_config.h"
#include "port_mapping.h"
#include "timer.h"
#include "system.h"

GPIO_InitTypeDef        GPIO_InitStructure;
extern struct input_port_mapping inps[MAX_INPUT_PORTS];
extern struct system_params sys_info;
uint8_t switch_flag;
extern uint8_t timer_flag;
extern uint8_t ten_milli_sec_cnt ;
uint8_t state_flag=0;
uint8_t ext_flag=0;


   
	  
	  
	  /* Clear the EXTI line 15 pending bit */

//	if(EXTI_GetITStatus(EXTI_Line7) != RESET) {
//		inps [3].curr_state = read_input_pin (3);
//		sys_info.check_di_debounce |= 0x08;
////		pin_int_ctrl (3, PORT_STATE_OFF);
//		EXTI_ClearITPendingBit(EXTI_Line7);
//	}else
//	if(EXTI_GetITStatus(EXTI_Line6) != RESET) {
//		inps [2].curr_state = read_input_pin (2);
//		sys_info.check_di_debounce |= 0x04;
////		pin_int_ctrl (2, PORT_STATE_OFF);
//		EXTI_ClearITPendingBit(EXTI_Line6);
//	}else
//	if(EXTI_GetITStatus(EXTI_Line5) != RESET) {
//		inps [1].curr_state = read_input_pin (1);
//		sys_info.check_di_debounce |= 0x02;
////		pin_int_ctrl (1, PORT_STATE_OFF);
//		EXTI_ClearITPendingBit(EXTI_Line5);
//	}else
//	if(EXTI_GetITStatus(EXTI_Line4) != RESET) {
//		inps [0].curr_state = read_input_pin (0);
//		sys_info.check_di_debounce |= 0x01;	
////		pin_int_ctrl (0, PORT_STATE_OFF);
//		EXTI_ClearITPendingBit(EXTI_Line4);
//	}
//	TIM_Cmd(TIM2, ENABLE);


//void EXTI0_1_IRQHandler(void) {
////	if(EXTI_GetITStatus(EXTI_Line0) != RESET) {								
////		/* Clear the EXTI line 0 pending bit */
////		EXTI_ClearITPendingBit(EXTI_Line0);
////	}
//	if(EXTI_GetITStatus(EXTI_Line1) != RESET) {
//		/* Clear the EXTI line 0 pending bit */
//		EXTI_ClearITPendingBit(EXTI_Line1);
//	}
//}

//void EXTI2_3_IRQHandler(void) {
//	if(EXTI_GetITStatus(EXTI_Line2) != RESET) {								
////		pin_int_ctrl (SOLAR_DIN7_PORT, SOLAR_DIN7_PIN, 0);
//		EXTI_ClearITPendingBit(EXTI_Line2);
//	}
////	if(EXTI_GetITStatus(EXTI_Line3) != RESET) {
////		/* Clear the EXTI line 0 pending bit */
////		EXTI_ClearITPendingBit(EXTI_Line3);
////	}
//}

void set_input_pin (GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
	EXTI_InitTypeDef EXTI_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	uint8_t pin = 0;
	uint16_t curr_pin = GPIO_Pin;

	if (GPIOx == GPIOB) {
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	}

	/* Configure PA0 pin as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOx, &GPIO_InitStructure);

	while (GPIO_Pin) {
		GPIO_Pin >>= 1;
		pin++;
	}
	pin--;
	if (GPIOx == GPIOB) {
		SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, pin);
	}

	EXTI_InitStructure.EXTI_Line = curr_pin;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	if (curr_pin == GPIO_Pin_0 || curr_pin == GPIO_Pin_1) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI0_1_IRQn;
	} else 
	if (curr_pin == GPIO_Pin_2 || curr_pin == GPIO_Pin_3) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI2_3_IRQn;
	} else {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
	}
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

uint8_t read_input_pin (uint8_t pin_no) {
	uint8_t ret = 0;
	switch (pin_no) {
		case 0:
			ret = GPIO_ReadInputDataBit (DI1_PORT, DI1_PIN);
			break;
		case 1:
			ret = GPIO_ReadInputDataBit (DI2_PORT, DI2_PIN);
			break;
		case 2:
			ret = GPIO_ReadInputDataBit (DI3_PORT, DI3_PIN);
			break;
		case 3:
			ret = GPIO_ReadInputDataBit (DI4_PORT, DI4_PIN);
			break;
	}
	return ret;
}

void pin_int_ctrl (uint8_t idx, uint8_t flag) {
	EXTI_InitTypeDef EXTI_InitStructure;
	uint16_t GPIO_Pin;
	
	switch (idx) {
		case 0:
			GPIO_Pin = DI1_PIN;
			break;
		case 1:
			GPIO_Pin = DI2_PIN;
			break;
		case 2:
			GPIO_Pin = DI3_PIN;
			break;
		case 3:
			GPIO_Pin = DI4_PIN;
			break;
	}

	EXTI_InitStructure.EXTI_Line = GPIO_Pin;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	if (flag)
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	else
		EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStructure);
}

/**
  * @brief  Configure PA0 in interrupt mode
  * @param  None
  * @retval None
  */
void init_di (void) {
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	set_input_pin (DI1_PORT, DI1_PIN);
	set_input_pin (DI2_PORT, DI2_PIN);
	set_input_pin (DI3_PORT, DI3_PIN);
	set_input_pin (DI4_PORT, DI4_PIN);

	timer_init(TIM2, TIME_MILLI_SEC, 50);
}

void set_output_pin (GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
	GPIO_InitTypeDef GPIO_InitStructure;

	if (GPIOx == GPIOA) {
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	}else 
	if (GPIOx == GPIOB) {
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	}

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOx, &GPIO_InitStructure);
}

void init_do (void) {
	set_output_pin (DO1_DRIVE_PORT, DO1_DRIVE_PIN);
	set_output_pin (DO2_DRIVE_PORT, DO2_DRIVE_PIN);
	set_output_pin (DO3_DRIVE_PORT, DO3_DRIVE_PIN);
	set_output_pin (DO4_DRIVE_PORT, DO4_DRIVE_PIN);
	set_output_pin (SYS_LED_DRIVE_PORT, SYS_LED_DRIVE_PIN);
	set_output_pin (COMM_LED_DRIVE_PORT, COMM_LED_DRIVE_PIN);
	set_output_pin (WP_DRIVE_PORT, WP_DRIVE_PIN);
	set_output_pin (DEN_DRIVE_PORT, DEN_DRIVE_PIN);
	DEN_OFF;

/*
	DO1_ON;
	DO1_OFF;

	DO2_ON;
	DO2_OFF;

	DO3_ON;
	DO3_OFF;
	  
	DO4_ON;
	DO4_OFF;

	DO5_ON;
	DO5_OFF;

	DO6_ON;
	DO6_OFF;
*/
}

void set_do (uint8_t outport, uint8_t level) {
	outport -= 64;
	switch (outport) {
		case 0:
			if (level) {
				DO1_ON;
			}
			else {
				DO1_OFF;
			}
			break;
		case 1:
			if (level) {
				DO2_ON;
			}
			else {
				DO2_OFF;
			}
			break;
		case 2:
			if (level) {
				DO3_ON;
			}
			else {
				DO3_OFF;
			}
			break;
		case 3:
			if (level) {
				DO4_ON;
			}
			else {
				DO4_OFF;
			}
			break;
	}
}

//void toggle_sys_led (void) {
//	if (GPIO_ReadOutputDataBit (SYS_LED_DRIVE_PORT, SYS_LED_DRIVE_PIN)) {
//		SYS_LED_OFF;
//	}
//	else {
//		SYS_LED_ON;
//	}
//}
//
//void toggle_com_led (void) {
//	if (GPIO_ReadOutputDataBit (COMM_LED_DRIVE_PORT, COMM_LED_DRIVE_PIN)) {
//		COMM_LED_OFF;
//	}
//	else {
//		COMM_LED_ON;
//	}
//}


