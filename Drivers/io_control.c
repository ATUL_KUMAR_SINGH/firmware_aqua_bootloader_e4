
#include <stdlib.h>
#include "stm32f0xx.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "io_control.h"

#include "std_periph_headers.h"


void Config_Pin_as_Output(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin_x)
{
		GPIO_InitTypeDef GPIO_InitStructure;
//------------------------------------------------------------------	
  if(GPIOx == GPIOA)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
  else if(GPIOx == GPIOB)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  
  else if(GPIOx == GPIOC)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

	else if(GPIOx == GPIOD)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
  
	else if(GPIOx == GPIOF)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOF, ENABLE);
//------------------------------------------------------------------	
		// Configure Pin in output pushpull mode 
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_x;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOx, &GPIO_InitStructure);
	
//------------------------------------------------------------------		
}



void Config_Pin_as_Input(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin_x)
{
		GPIO_InitTypeDef GPIO_InitStructure;
//------------------------------------------------------------------	
  if(GPIOx == GPIOA)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
  else if(GPIOx == GPIOB)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  
  else if(GPIOx == GPIOC)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

	else if(GPIOx == GPIOD)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
  
	else if(GPIOx == GPIOF)
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOF, ENABLE);
//------------------------------------------------------------------		
		GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_x;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_Init(GPIOx, &GPIO_InitStructure);
		
}

